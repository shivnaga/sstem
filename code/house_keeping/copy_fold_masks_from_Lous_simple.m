function copy_fold_masks_from_Lous_simple(config, layer_ids)
fprintf('START: copy_fold_masks_from_Lous_simple\n');

input_file_name = [config.align.Lous_alignment_directory, config.align.Lous_global_alignment_file];
input_fold_mask_dir = config.align.Lous_alignment_directory;
fold_mask_output_dir = get_fold_dir(config);

config.region.region_structure = get_region_structure(config);

for i = layer_ids
  tilt_plane = config.region.region_structure.planes(i).tilt_planes(1);
  grep_string = tilt_plane.tiles(1).sub_dir;
  for j = 1:length(tilt_plane.tiles)
    tile = tilt_plane.tiles(j);
    fprintf('tile: %s\n', tile.name);
    
    comm = ['grep ', grep_string, ' ', input_file_name, ' | grep "FOLDMAP" | grep "', tile.name, '" | uniq | ', ...
      'awk ''{print $3;}'''];
    [status, result] = system(comm);
    if(status~=0)
      error('Error while recovering fold mask file name');
    end
    result = result(1:end-1);
    fold_mask_filename = [input_fold_mask_dir, result];
    fold_mask_filename = [fold_mask_filename(1:end-4), 'd.tif'];
    fprintf('fold_mask_filename: %s\n', fold_mask_filename);
    fold_mask = imread(fold_mask_filename);
    
    n_patch = max(fold_mask(:));
    fprintf('n_patch: %d\n', n_patch);
    
    fold_mask_new = zeros(size(fold_mask));
    for patch_id = 1:n_patch
      mask = fold_mask==patch_id;
      mask = imfill(mask, 4, 'holes');
      fold_mask_new = max(fold_mask_new, double(mask)*double(patch_id));
    end
    fold_mask = uint8(fold_mask_new);
    
    check_for_dir([fold_mask_output_dir, tile.sub_dir]);
%     fold_mask_output_file_name = ...
%       [fold_mask_output_dir, tile.name, '.fold_mask.mat'];
%     save2(fold_mask_output_file_name, 'fold_mask');
    fold_mask_output_file_name = ...
      [fold_mask_output_dir, tile.name, '.fold_mask.tif'];
    imwrite(fold_mask, get_storage_file_name(fold_mask_output_file_name));
    
  end
end

fprintf('STOP: copy_fold_masks_from_Lous_simple\n');
return
end
