function display_aligned_tiles_in_plane(config)
% display_aligned_tiles_in_plane(config)
% Display aligned tile images using transformations computed for each
% plane/section separately. This is part of two stage alignment
%
% Shiv N. Vitaladevuni
% Janelia Farm Research Campus, HHMI
%
% v0  08312008  init. code
% v1  12152008  modified for multiple alignment methods
%

display_aligned_tiles_affine_in_plane(config);

return
end
