function make_mex_here()

mex -output get_segment_boundary_sets...
  CFLAGS="\$CFLAGS -O3" ...
  get_segment_boundary_sets.cpp

mex -output get_segment_boundary_sets_masked ...
  CFLAGS="\$CFLAGS -O3" ...
  get_segment_boundary_sets_masked.cpp

return
end
