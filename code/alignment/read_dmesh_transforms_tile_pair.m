function transforms_tp = read_dmesh_transforms_tile_pair(config, ...
  image_prefix_1, image_prefix_2, case_id_1, case_id_2)

dmesh_config = config.align.in_section_align.deformable_mesh;
dmesh_dir = get_deformable_mesh_tile_pair_dir(config);

transforms_tp = [];
if(isfield(dmesh_config, 'input_tform_format') && ...
    strcmp(dmesh_config.input_tform_format, 'tif_txt'))
  fprintf('Trying to read dmesh .tif and .txt files\n');
  % Read in piecewise transformations as a TIFF file of the map mask
  % and a text file of the affine transformations.
  file_name_suffix = get_file_name_from_tuple(dmesh_dir, ...
    image_prefix_1, image_prefix_2, 'dt.');
  file_name_tforms = get_storage_file_name([file_name_suffix, '.tforms.txt']);
  fprintf('file_name_tforms: %s\n', file_name_tforms);
  if(exist(file_name_tforms, 'file')==2)
    fprintf('transform .txt exists\n');
    transforms_tp = [];
    fin_tforms = fopen(file_name_tforms, 'rt');
    transforms_tp.transforms = fscanf(fin_tforms, '%g', [6, inf]);
    fclose(fin_tforms);
    if(isempty(transforms_tp.transforms))
      fprintf('transform .txt is empty\n');
      transforms_tp.map_mask = [];
      fprintf('transform file txt:\n%s\n', ...
        get_storage_file_name(file_name_tforms));
    else
      fprintf('Successfully read transform .txt file. Trying to read .tif file\n');
      file_name_map = get_storage_file_name([file_name_suffix, '.map.tif']);
      if(exist(file_name_map, 'file')==2)
        fprintf('Transform .tif file exists\n');
        transforms_tp.map_mask = imread(file_name_map);
        fprintf('transform file tif:\n%s\n', ...
          get_storage_file_name(file_name_map));
      else
        fprintf('Transform .tif file does not exist\n');
      end
    end
  else
    fprintf('transform .txt file does not exist\n');
  end
else
  if(isfield(dmesh_config, 'input_tform_format') && ...
      strcmp(dmesh_config.input_tform_format, 'tif_txt_Lou'))
    % Read in piecewise transformations as a TIFF file of the map mask
    % and a text file of the affine transformations.
    c_1 = strsplit(image_prefix_1, '/');
    c_2 = strsplit(image_prefix_2, '/');
    file_name_suffix = [dmesh_dir, num2str(case_id_1), '/', c_1{3}(1:3), '/', ...
      num2str(case_id_2), '.', c_2{3}(1:3)];
    file_name_tforms = get_storage_file_name([file_name_suffix, '.tf.txt']);
    fprintf('file_name_tforms: %s\n', file_name_tforms);
    if(exist(file_name_tforms, 'file')==2)
      fprintf('transform .txt exists\n');
      transforms_tp = [];
      fin_tforms = fopen(file_name_tforms, 'rt');
      transforms_tp.transforms = fscanf(fin_tforms, '%g', [6, inf]);
      fclose(fin_tforms);
      if(isempty(transforms_tp.transforms))
        fprintf('transform .txt is empty\n');
        transforms_tp.map_mask = [];
        fprintf('transform file txt:\n%s\n', ...
          get_storage_file_name(file_name_tforms));
      else
        fprintf('Successfully read transform .txt file. Trying to read .tif file\n');
        file_name_map = get_storage_file_name([file_name_suffix, '.map.tif']);
        if(exist(file_name_map, 'file')==2)
          fprintf('Transform .tif file exists\n');
          transforms_tp.map_mask = imread(file_name_map);
          fprintf('transform file tif:\n%s\n', ...
            get_storage_file_name(file_name_map));
        else
          fprintf('Transform .tif file does not exist\n');
        end
      end
    else
      fprintf('transform .txt file does not exist\n');
    end
  else
    fprintf('Trying to read dmesh .mat file\n');
    % Read in piecewise transformations as a MATLAB .mat file
    file_name_suffix = get_file_name_from_tuple(dmesh_dir, ...
      image_prefix_1, image_prefix_2, 'dt.');
    file_name = [file_name_suffix, '.mat'];
    try
      load2(file_name,'transforms_tp');
      fprintf('transform file mat:\n%s\n', ...
        get_storage_file_name(file_name));
      fprintf('Successfully read dmesh .mat file\n');
    catch %#ok<CTCH>
      fprintf('Could not read dmesh .mat file\n%s\n', ...
        get_storage_file_name(file_name));
      transforms_tp = [];
    end
  end
end

return
end