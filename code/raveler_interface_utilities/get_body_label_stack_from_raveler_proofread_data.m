function body_stack = get_body_label_stack_from_raveler_proofread_data(...
  proofread_data_dir, planes, myvarargin)
% body_stack = get_body_label_stack_from_raveler_proofread_data(...
%   proofread_data_dir, planes)
% Construct a body label stack from exported proofread data from Raveler.
% Inputs:
%   proofread_data_dir    directory to which data was exported to by
%                           Raveler.
%   planes                sequence of planes in the stack
% Output:
%   body_stack            3D volume of body labels.
%
% Shiv N. Vitaladevuni
% Janelia Farm Research Campus, HHMI.
%
% v0  06192009  init. code
%

superpixel_map_dir = 'superpixel_maps/';
superpixel_map_prefix = 'sp_map.%05d';
superpixel_map_suffix = '.png';
superpixel_2_segment_map_file = 'superpixel_to_segment_map.txt';
segment_2_body_map_file = 'segment_to_body_map.txt';
is_numbered_from_1 = true;
is_sstem = true;
is_verbose = false;
if(nargin<=2)
  myvarargin = {};
end
for i = 1:2:length(myvarargin)
  fprintf('myvarargin: %s %s\n', myvarargin{i}, myvarargin{i+1});
  switch(myvarargin{i})
    case 'superpixel_map_dir'
      superpixel_map_dir = myvarargin{i+1};
    case 'superpixel_map_prefix'
      superpixel_map_prefix = myvarargin{i+1};
    case 'superpixel_map_suffix'
      superpixel_map_suffix = myvarargin{i+1};
    case 'superpixel_2_segment_map_file'
      superpixel_2_segment_map_file = myvarargin{i+1};
    case 'segment_2_body_map_file'
      segment_2_body_map_file = myvarargin{i+1};
    case 'is_numbered_from_1'
      is_numbered_from_1 = eval(myvarargin{i+1});
    case 'is_sstem'
      is_sstem = eval(myvarargin{i+1});
    case 'is_verbose'
      is_verbose = eval(myvarargin{i+1});
  end
end

if is_verbose,
    fprintf('START: get_body_label_stack_from_raveler_proofread_data\n');
    fprintf('superpixel to segment file name: %s\n', [proofread_data_dir, ...
        superpixel_2_segment_map_file]);
end
fin_sp_2_seg = fopen([proofread_data_dir, superpixel_2_segment_map_file], 'rt');
sp_2_seg = fscanf(fin_sp_2_seg, '%d', [3, inf])';
fclose(fin_sp_2_seg);

if is_verbose,
    fprintf('segment to body file name: %s\n', [proofread_data_dir, ...
        segment_2_body_map_file]);
end

fin_seg_2_body = fopen([proofread_data_dir, segment_2_body_map_file], 'rt');
seg_2_body = fscanf(fin_seg_2_body, '%d', [2, inf]);
segment_2_body_map = zeros(max(seg_2_body(1,:))+1,1);
segment_2_body_map(seg_2_body(1,:)+1) = seg_2_body(2,:);
fclose(fin_seg_2_body);

size_sp =   size(imread([proofread_data_dir, superpixel_map_dir, ...
    sprintf(superpixel_map_prefix, planes(1)), superpixel_map_suffix]));
body_stack = uint32(zeros([size_sp, length(planes)]));
for p = 1:length(planes)
  if(~is_numbered_from_1)
    plane = planes(p);
  else
    plane = p;
  end
  if is_verbose,
    fprintf('plane: %d\n', plane);
  end

  superpixel_map = imread([proofread_data_dir, superpixel_map_dir, ...
    sprintf(superpixel_map_prefix, plane), superpixel_map_suffix]);
  
  sp_2_seg_p = sp_2_seg(sp_2_seg(:,1)==plane, 2:3);
  segment_map = apply_mapping(superpixel_map, sp_2_seg_p);
  
  body_map = segment_2_body_map(1+segment_map);
  if(is_sstem)
    body_map = remove_merged_boundaries_2D(uint32(body_map));
  end
  body_stack(:,:,p) = body_map;
end

if(~is_sstem)
  body_stack = remove_merged_boundaries_3D(uint32(body_stack));
end

if is_verbose,
    fprintf('STOP: get_body_label_stack_from_raveler_proofread_data\n');
end

end
