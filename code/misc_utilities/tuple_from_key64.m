function tuple_keys = tuple_from_key64(keys)
% assumes 2^20 maximum keys supported
    keys = double(keys);
    tuple_keys = [floor(keys/2^20), rem(keys, 2^20)];
end