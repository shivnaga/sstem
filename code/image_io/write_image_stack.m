function write_image_stack(stack, filename, params)
% WRITE_IMAGE_STACK: Write a stack of images to disk, either as a series of
%    8-bit TIFFs or PNGs, or as a single, multi-image TIFF. The series can
%    be either grayscale (size [m n p]) or RGB color (size [m n 3 p])
% INPUT:
%    - stack: the stack, a 3-dimensional matrix
%    - params: the writing parameters:
%        * directory: the file system directory in which to write the files
%        * type: the type of image file, e.g. 'tif' or 'png'. Note that
%        single_file only works for 'tif'.
%        * bitdepth: the number of bits per pixel (only relevant for stacks
%        of class 'double' (default: 8);
%        * first_filenum: the first number in the series. (stack(:,:,1) will be
%        written as the first_filenum^th file)
%        * first_slice: the first slice in the image stack to be written to
%        disk
%        * num_slices: the last number in the series.
%        (stack(:,:,first_slice+num_slices-1) will be written as the last file
%        * single_file: Whether the stack should be written as multiple, 
%        sequential TIFFs or a single, multi-image TIFF
%        * compression: should the files be compressed (directly passed to 
%        Matlab's imwrite function)
%        * invert: should the image files be inverted? (img_out=1-img_in)
%        * adjust: enforce that the image data is in [0,1] (default is
%        true)
%        * histeq: perform histogram equalization before writing

    %% Preprocessing of the parameters and default value assignments
    if nargin < 3,
        params = struct();
    end
    
    if isfield(params, 'invert') && params.invert,
        if isa(stack, 'double'),
            stack = 1-stack;
        elseif isa(stack, 'uint8'),
            stack = 255-stack;
        elseif isa(stack, 'uint16'),
            stack = 65535-stack;
        end
    end
    
    if isfield(params, 'compression'),
        compression = params.compression;
    else
        compression = 'none';
    end

    if ~isfield(params, 'single_file'),
        params.single_file = false;
    end

    if isempty(find(filename == '%', 1)),
        params.single_file = true;
    end
    
    seps = strfind(filename, '/');
    if ~isempty(seps),
        directory_in_fn = [filename(1:(seps(end)-1)) '/'];
        filename = filename((seps(end)+1):end);
    else
        directory_in_fn = '';
    end
    
    if ~isfield(params, 'type'),
        if strcmp(filename(end-3:end), '.png'),
            params.type = 'png';
        elseif strcmp(filename(end-3:end), '.tif') || ...
                strcmp(filename(end-4:end), '.tiff'),
            params.type = 'tif';
        else
            params.type = 'tif';
        end
    end
    
    if ~isfield(params, 'bitdepth'),
        params.bitdepth = 8;
    end
    
    if ~isfield(params, 'adjust'),
        params.adjust = false;
    end
    if ~isfield(params, 'histeq'),
        params.histeq = false;
    end
    
    if isfield(params, 'directory'),
        directory = [params.directory '/' directory_in_fn];
    else
        directory = directory_in_fn;
    end
    
    if ~isfield(params, 'first_slice'),
        params.first_slice = 1;
    end
    
    if ~isfield(params, 'num_slices'),
        params.num_slices = size(stack, ndims(stack));
    end
    
    if isfield(params, 'first_filenum'),
        first_filenum = params.first_filenum;
        last_filenum = first_filenum+params.num_slices-1;
    else
        first_filenum = 0;
        last_filenum = first_filenum+params.num_slices-1;
    end
    
    s = size(stack);
 
    % if we are using 'double' for the image data, rescale if necessary to
    % [0, 1] before converting for output.    
    if isa(stack, 'double'),
        if min(stack(:)) < 0,
            stack = stack - min(stack(:));
        end
        if max(stack(:)) > 1,
            stack = stack / max(stack(:));
        end
    end
    
    if ndims(stack) == 3,
        color_image = false;
        stack = reshape(stack, [s(1) s(2) 1 s(3)]);
    elseif ndims(stack) == 4,
        color_image = true;
    end
    
    s = size(stack);
    if params.adjust,
        stackp = permute(stack, [1 2 4 3]);
        stackp = reshape(stackp, [s(1) s(2)*s(4) s(3)]);
        stackp = reshape(imadjust(stackp, stretchlim(stackp, 0)), ...
                                 [s(1) s(2) s(4) s(3)]);
        stack = permute(stackp, [1 2 4 3]);
        clear stackp;
    end
    
    if params.histeq,
        stackp = permute(stack, [1 2 4 3]);
        stackp = reshape(stackp, [s(1) s(2)*s(4) s(3)]);
        stackp = reshape(histeq(stackp), [s(1) s(2) s(4) s(3)]);
        stack = permute(stackp, [1 2 4 3]);
        clear stackp;
    end
    
    if isa(stack, 'double'),
        if params.bitdepth == 8,
            stack = uint8(255*stack);
        elseif params.bitdepth == 16,
            stack = uint16(65535*stack);
        end
    end


    %% Writing of the files
    if params.single_file,
        if ~color_image, % B&W image
            cur_image = squeeze(stack(:,:,params.first_slice));
        else % color image
            cur_image = squeeze(stack(:,:,:,params.first_slice));
        end
        imwrite(cur_image, [directory filename], params.type, 'Compression', ...
            compression);
        for filenum=(first_filenum+1):last_filenum,
            if ~color_image, % B&W image
                cur_image = squeeze( ...
                        stack(:,:,filenum-first_filenum+params.first_slice));
            else % color image
                cur_image = squeeze( ...
                        stack(:,:,:,filenum-first_filenum+params.first_slice));
            end
            imwrite(cur_image, [directory filename], params.type, ...
                'WriteMode', 'append', 'Compression', compression);
        end
    else
        for filenum=first_filenum:last_filenum,
            fn = sprintf(filename, filenum);
            if color_image,
                cur_image = squeeze( ...
                        stack(:,:,:,filenum-first_filenum+params.first_slice));
            else
                cur_image = squeeze( ...
                        stack(:,:,filenum-first_filenum+params.first_slice));
            end
            imwrite(cur_image, [directory fn], params.type, ...
                'Compression', compression);
        end
    end
end
