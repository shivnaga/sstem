function subs = myind2sub(sz, inds)
    d = numel(sz);
%    p = ones([d, 1]);
%    for i=2:d,
%        p(i) = prod(sz(1:(i-1)));
%    end
    p = [1 cumprod(sz(1:end-1))];
    n = numel(inds);
    subs = zeros([n d]);
    for k=d:-1:1,
        r = rem(inds-1, p(k))+1;
        z = (inds - r)/p(k) + 1;
        subs(:, k) = z;
        inds = r;
    end
end