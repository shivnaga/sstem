function mask = mask_from_probability_map(pmap, threshold, size_threshold)
    pmap = bwconncomp(uint8(pmap > threshold));
    for k=1:numel(pmap.PixelIdxList),
        if numel(pmap.PixelIdxList{k}) < size_threshold,
            pmap.PixelIdxList{k} = [];
        end
    end
    
    mask = logical(labelmatrix(pmap));
end