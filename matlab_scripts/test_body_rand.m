tgt = ones(9);
tgt(:,5) = 0;
tgt(5,1:5) = 0;
tgt = bwlabel(tgt, 4);

tas = ones(9);
tas(:,5) = 0;
tas(7,5:9) = 0;
tas = bwlabel(tas, 4);

tbr_params = struct('markers_per_body', 5, 'min_body_size', 1, ...
    'submatrix_size', 15, 'diff', true, ...
    'diff_markers', true, 'verbose', true, 'debug', true);

[tconf, tdiff] = body_rand(tgt, tas, tbr_params);