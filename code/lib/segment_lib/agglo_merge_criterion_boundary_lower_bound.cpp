// merge criteria for agglomerative segmentation: mean boundary value
//
// Shiv N. Vitaladevuni
// Janelia Farm Research Campus, HHMI.
//

#include <iostream>
#include <cmath>

#include <segment.h>
#include <agglo_merge_criterion_boundary_lower_bound.h>



#define PARENT_CLASS__ Agglo_Merge_Criterion_Boundary_Lower_Bound

#define COLLECT_BOUNDARY_STATISTICS__ {                             \
        sum_boundary_values[LP_2_LPK(Label_Pair(s1,s2))] += *b_ptr; \
        sum_boundary_values_squared[LP_2_LPK(Label_Pair(s1,s2))] += (*b_ptr) * (*b_ptr); \
        n_boundary_values[LP_2_LPK(Label_Pair(s1,s2))] ++;                    \
    }

#define COLLECT_INTERNAL_STATISTICS__ {}

#include <agglo_merge_criterion_initialize_adjacency_statistics_2D.h>
#include <agglo_merge_criterion_initialize_adjacency_statistics_3D.h>



namespace iput
{
    namespace seg
    {
        Agglo_Merge_Criterion_Boundary_Lower_Bound::Agglo_Merge_Criterion_Boundary_Lower_Bound(double a) :
        t_alpha(a) {
        }

        bool Agglo_Merge_Criterion_Boundary_Lower_Bound::initialize_adjacency_statistics(
            const Array<Boundary_Value> * boundary_map,
            const Array<Label> * segment_map){

            if(boundary_map->n_dimension==2){
                bool is_valid = initialize_adjacency_statistics_2D__(boundary_map, segment_map);
                return is_valid;
            }
            if(boundary_map->n_dimension==3){
                bool is_valid = initialize_adjacency_statistics_3D__(boundary_map, segment_map);
                return is_valid;
            }

            return false;
        }
        
        Merge_Priority Agglo_Merge_Criterion_Boundary_Lower_Bound::get_merge_priority(Label v_i, Label v_j){
            Label_Pair_Key lpk = LP_2_LPK(Label_Pair(v_i,v_j));
            double n = static_cast<double>(n_boundary_values[lpk]);
            if(n >= 2.0) {
                double xbar = static_cast<double>(sum_boundary_values[lpk])/n;
                double x2 = static_cast<double>(sum_boundary_values_squared[lpk]);
                double var = std::max(x2/(n-1) - n/(n-1)*xbar*xbar, 0.0);
                double sem = sqrt(var/n);
                return (Merge_Priority) (xbar - t_alpha*sem);
            }
            else
                return (Merge_Priority) 100000;
        }
        
        bool Agglo_Merge_Criterion_Boundary_Lower_Bound::should_be_merged(Label v_i, Label v_j, Merge_Priority f_threshold){
            return get_merge_priority(v_i, v_j)<f_threshold;
        }
        
        void Agglo_Merge_Criterion_Boundary_Lower_Bound::unite_pairwise_statistics(Label n, Label source, Label target){
            Label_Pair_Key lpk_ns = LP_2_LPK(Label_Pair(n,source));
            Label_Pair_Key lpk_nt = LP_2_LPK(Label_Pair(n,target));
            sum_boundary_values[lpk_nt] += sum_boundary_values[lpk_ns];
            sum_boundary_values_squared[lpk_nt] += sum_boundary_values_squared[lpk_ns];
            n_boundary_values[lpk_nt] += n_boundary_values[lpk_ns];
        }
        
        void Agglo_Merge_Criterion_Boundary_Lower_Bound::move_pairwise_statistics(Label n, Label source, Label target){
            Label_Pair_Key lpk_ns = LP_2_LPK(Label_Pair(n,source));
            Label_Pair_Key lpk_nt = LP_2_LPK(Label_Pair(n,target));
            sum_boundary_values[lpk_nt] = sum_boundary_values[lpk_ns];
            sum_boundary_values_squared[lpk_nt] = sum_boundary_values_squared[lpk_ns];
            n_boundary_values[lpk_nt] = n_boundary_values[lpk_ns];
        }
        void Agglo_Merge_Criterion_Boundary_Lower_Bound::unite_unary_statistics(Label source, Label target) {
            return;
        }
    }    
}
