function [npv, spec] = brcurve_from_raveler(gt, ts, as_path, planes, params, cv)

    if nargin < 6,
        cv = {1:size(gt, 1), 1:size(gt, 2), 1:size(gt, 3)};
    end
    if nargin < 5,
        params = struct();
    end
    if nargin < 4,
        planes = 1:16;
    end
    
    if ~isfield(params, 'name'),
        params.name = '';
    end
    
%     if ~isfield(params, 'png_padding'),
%         png_format_str = '%05i';
%     else
%         png_format_str = ['%0' sprintf('%i', params.png_padding) 'i'];
%     end
    
    if ~isfield(params, 'map_padding'),
        map_format_str = '%03i';
    else
        map_format_str = ['%0' sprintf('%i', params.map_padding) 'i'];
    end
    
    if ~isfield(params, 'reps'),
        reps = 1;
    else
        reps = params.reps;
    end
    
    if ~isfield(params, 'br_params'),
        br_params = struct('markers_per_body', 20, 'min_body_size', 1500, 'is2d', true, 'diff', 0, 'debug', 0, 'verbose', 0);
    else
        br_params = params.br_params;
    end
    
    conf = zeros([reps numel(ts) 2 2]);
    for k=1:reps,
        h = waitbar(0, sprintf(['running ' params.name ' evaluation number %i'], k));
        for i=1:numel(ts),
            sp2seg = sprintf(['superpixel_to_segment_map.' map_format_str '.txt'], ts(i));
            seg2bod = sprintf(['segment_to_body_map.' map_format_str '.txt'], ts(i));
            seg = get_body_label_stack_from_raveler_proofread_data(as_path, ...
                planes, {'superpixel_2_segment_map_file', sp2seg, ...
                'segment_2_body_map_file', seg2bod});
            [conf(k,i,:,:), ~] = body_rand(gt(cv{:}), seg(cv{:}), br_params);
            waitbar(i/numel(ts), h);
        end
        delete(h)
    end
    
    mean_conf = squeeze(mean(conf, 1));
    
    npv = mean_conf(:,2,2) ./ (mean_conf(:,2,2) + mean_conf(:,2,1));
    spec = mean_conf(:,2,2) ./ (mean_conf(:,2,2) + mean_conf(:,1,2));
end
