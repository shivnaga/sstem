basedir = '/groups/chklovskii/home/nuneziglesiasj/data/tao3/test-error/';
params = struct();
params.dir = [basedir '../../tao2/test-error/'];
params.padding = 3;

[t2npv, t2spec] = brcurve(tilt_gt_orig, 3:3:249, 'bm', '.amb_', '', ...
                                            params, {280:548, 11:548, 11:70});
[tp2npv, tp2spec] = brcurve(tilt_gt_orig, 3:3:249, 'bmp', '.amb_', '', ...
                                            params, {280:548, 11:548, 11:70});
params.dir = basedir;
[t3npv, t3spec] = brcurve(tilt_gt, 3:3:249, 'bm', '.amb_', '', ...
                                            params, {280:548, 11:548, 11:70});
[tp3npv, tp3spec] = brcurve(tilt_gt, 3:3:249, 'bm', '.amb_', '', ...
                                            params, {280:548, 11:548, 11:70});
