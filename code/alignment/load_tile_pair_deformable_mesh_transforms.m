function [transforms_tp, transforms_tp_rev] = ...
  load_tile_pair_deformable_mesh_transforms(config, ...
  image_prefix_1, image_prefix_2, case_id_1, case_id_2, ...
  size_image_1, size_image_2)

global config_global

if(isfield(config.align.linkage_align, 'method') && ...
    ~isempty(config.align.linkage_align.method))
  linkage_align_method = config.align.linkage_align.method;
else
  linkage_align_method = 'prealign';
end

switch(linkage_align_method)
  case 'deformable_mesh'
    transforms_tp = read_dmesh_transforms_tile_pair(config, ...
      image_prefix_1, image_prefix_2, case_id_1, case_id_2);

    transforms_tp_rev = read_dmesh_transforms_tile_pair(config, ...
      image_prefix_2, image_prefix_1, case_id_2, case_id_1);
  case 'prealign'
    transforms_tp.map_mask = config_global.TRANSFORMATION_ID_OFFSET*ones(size_image_1);
    transforms_tp.transforms = [1 0 0 1 0 0];
    transforms_tp_rev.map_mask = config_global.TRANSFORMATION_ID_OFFSET*ones(size_image_2);
    transforms_tp_rev.transforms = [1 0 0 1 0 0];
  otherwise
    error('config.align.linkage_align.method not recognized');
end
return
end
