function seg_eval_medulla_Leginon_3500x_fall08_misc_BEL_amb_l_abifl_seg(module_id)
% seg_eval_medulla_Leginon_3500x_fall08_misc_BEL_amb_l_abifl(module_id)
% Evaluate segmentation boundary on sec. 0 and 2 of stack
% medulla.HPF.Leginon.3500x.zhiyuan.fall2008.seg_eval.
% Algo: BEL agglo. mean boundary + ladder + ABIFL
%
% module_id
%   1     mitochondria_manual_annotation(config);
%   2     mitochondria_collect_training_samples_intensity_histograms(config);
%   3     mitochondria_train_boosted_intensity_hist_detector(config);
%   4     mitochondria_apply_boosted_intensity_hist_detector(config);
%   5     segment_2D_grayscale_ladder(config);
%   6     superpixel_2_segment_grayscale_ladder(config);
%   7     linkage_3D_train_intensity_pair_boost(config);
%   8     linkage_3D_gen_linkage_gph_intensity_pair_boost(config);
%   9     generate_al(config);
%         generate_cat(config);
%         generate_superpixel_2_seg_map(config);
%         process_superpixel_2_2Dseg_linkage_graph(config);
%   10    prepare_final_reconstruction_volume(config);
%   11    save_reconstruction_info(config);
%         save_reconstruction_param_to_xml(config);
%   12    get_q_score(config);
%   13    get_rand_score(config);
%   14    get_boundary_match_score(config);
% 
% Shiv N. Vitaladevuni
% Janelia Farm Research Campus, HHMI
%
% v0  04162008  init code
% v1  05062008  code borrowed from reconstruction script
%               Marta's reconstruction_lamina_OTO_Jan222008_ministack.m
% v2  05072008  Modified for segmentation evaluation
% v3  06042008  Modified for medulla HPF FS
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% I. Constants - Not to be changed by users
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
config = get_basic_config();

% % Whether to use hashing when generating file names during storage and
% % retrieval. This reduces the filename lengths.
% global config_global
% config_global.storage.file_name_is_hashed__ = false;

config.stack.dir = '/groups/chklovskii/medulla/';

config.DEBUG = false;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% II. Configuration parameters - to be set by user
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1. Stack parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%
% (a) stack name
config.stack.name = 'medulla.HPF.Leginon.3500x.zhiyuan.fall2008';   
% (b) image name:
% If the files are image_0001.tif, image_0002.tif, etc., then
% the image_prefix would be "image_%04d" and the image_suffix would be
% ".tif".
% If the files are a001.tif, a002.tif, etc., then
% the image_prefix would be "a%03d" and the image_suffix would be
% ".tif".
config.stack.image_prefix = 'a.%03d';  
config.stack.image_suffix = '.tif';
% (c) slice ids: If the images are a001.tif, ..., a010.tif then the
% case_ids are 1,2,...,10.
config.stack.case_ids = 7:11;
% Option 2: If the images correspond to a trakEM project then
% * specify the trakEM file structure xml file name
% % % config.stack.image_structure = 'crop4_global_alignment_0161_0860.xml';
% % % % * slice ids: specify the z-plane numbers.
% % % config.stack.case_ids = 443;
% (d) ROI: Region Of Interest for the stack. If the entire slice image is
% to be processed then assign empty ([]). Otherwise get_roi(config);
config.stack.roi = [];
% config.stack.roi.xmin = 101;
% config.stack.roi.xmax = 1800;
% config.stack.roi.ymin = 101;
% config.stack.roi.ymax = 1800;
% config.stack.roi = get_roi(config);
% (e) Alignment parameters to describe the registration of different slices
% with respect to each other. The alignment should be done in IMOD or
% equivalent program to generate a .xf file.
% A stack is prealigned if the images in the stack's directory are
% already aligned to each other. If the stack is not prealigned then the
% 2D segmentation (superpixel and superpixel-to-segments) is performed on
% the unaligned images. The alignment parameters are used for linkage and
% to construct the proofreading data structures.
% * If the stack is prealigned then set is_prealigned as true else false
config.stack.align.is_prealigned = true;
% * If not prealigned then name the .xf file specifying the alignment.
% Assumed to be located in the stack directory.
% config.stack.align.xf_file_name = '1.xf';
% config.stack.align.xg_file_name = '1.xg';
% config.stack.align.margin = 200;
% config.stack.align.roi = [];
% (f) Stack resolution parameters for area and boundary histograms. These
% are to be used if the models having been trained on images of resolution
% different from the current one. This might improve the results,
% especially during boot-strapping.
config.stack.length_factor = 1;
config.stack.area_factor = config.stack.length_factor.^2;
% () Set the scale at which segmentation should be performed. This is kept
% constant for all superpixel and superpixel-to-seg stages. When stitching
% the segmentations, the label maps can be further downsampled if needed,
% but not artificially upsampled. During linkage and proofreading, the
% segmentations are upsampled to original resolution.
% config.stack.segmentation_scale = 2;
% config.stack.segmentation_scaling_method = 'bilinear';

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Reconstruction 
%%%%%%%%%%%%%%%%%%%%%%%%%%
% (a) name for the reconstruction
config.reconstruction.name = 'analysis1';

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. Mitochondria
%%%%%%%%%%%%%%%%%%%%%%%%%%
% (a) Training image name:
% If the files are a001.tif, a002.tif, etc., then
% the image_prefix would be "a%03d" and the image_suffix would be
% ".tif".
config.mitochondria.train.image_prefix = 'mitochondria.%03d';
config.mitochondria.train.image_suffix = '.tif';
% (b) image ids: : If the images are a006.tif, ..., a010.tif then the
% case_ids are 6,...,10.
config.mitochondria.train.case_ids = 1:3;
% (c) For constructing the feature vector for mitochondria detection. In
% case intensity histograms, the patch size is (2xwindows_size+1) X (2xwindows_size+1).
% The intensity histogram's bins are specified in intensity_bins
config.mitochondria.feature.type = 'heq_intensity_hist';
config.mitochondria.feature.window_sizes = [4 10 20 30]; % 15 25 35
config.mitochondria.feature.intensity_bins = 0:0.1:1;
% (d) Classifier type
config.mitochondria.model.type = 'boost';
% (e) Number of iterations of boosting
config.mitochondria.model.n_iteration = 30; % 30
% (f) Tree depth
config.mitochondria.model.tree_depth = 2; % 1

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4. Vesicle
%%%%%%%%%%%%%%%%%%%%%%%%%%
config.vesicle.apply.dir = '';
config.vesicle.apply.save_suffix = '';

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5. Superpixel - multi stage
%%%%%%%%%%%%%%%%%%%%%%%%%%
config.superpixel = repmat(config.superpixel, [2 1]);
%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.1 Superpixel - agglomerative mean boundary
%%%%%%%%%%%%%%%%%%%%%%%%%%
% (a) Algorithm to be used - must match with the function called
config.superpixel(1).method = 'grayscale_AglMeanB';
% () thresholds on the boundary field
config.superpixel(1).f_threshold_seq = 0.01:0.01:0.1;
% () thresholds on the boundary field for which segmentations should be
% saved. Useful for reducing disk space.
config.superpixel(1).save_f_thresholds = 0.1;
% Whether to save the segmentation overlaps as TIF files for viewing.
config.superpixel(1).save_segmentation_overlay = true;
% () minimum boundary length (pixel units) for two segments to be merged
% should increase with increasing f_thresholds. E.g., 0 for f_thresholds
% <=0.65 and 20 otherwise
config.superpixel(1).length_threshold_seq = ...
  25*(config.superpixel(1).f_threshold_seq>0.65);
% () Whether mitochondria are to be used to suppress false boundaries
config.superpixel(1).use_mitochondria = true;
% () The confidence threshold to be applied on the mitochondria
config.superpixel(1).mitochondria_confidence_threshold = 0; 
% () The thresholded mitochondria mask is eroded to ensure that correct
% boundaries are not obliterated
config.superpixel(1).mitochondria_erosion = 5; % 20
% * Minimum of a thresholded connected component for it to be considered
% mitochondria
config.superpixel(1).mitochondria_min_area = 40;
% () Whether to use vesicle detection to suppress false boundaries
config.superpixel(1).use_vesicle = false;
% () The threshold for the vesicle detection confidence
config.superpixel(1).vesicle_threshold = 0;
% () Radius of the vesicles. Boundaries within this distance from detected
% vesicle centers are obliterated.
config.superpixel(1).vesicle_dilation = 0;
% Image filtering for boundary detection to be used for watershed
% boundary = 1 - filtered_image
config.superpixel(1).watershed_filter_version = 'BELc\_5\_mf7_e6_d4';
% () Image filtering before performing segmentation (affects boundary
% detection).
config.superpixel(1).filter_version = 'BELc\_5\_mf7_e6_d4';
% () whether to display intermediate results
config.superpixel(1).is_verbose = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.2 Superpixel - grayscale ladder
%%%%%%%%%%%%%%%%%%%%%%%%%%
% (a) Algorithm to be used - must match with the function called
config.superpixel(2).method = 'grayscale_ladder';
% () Superpixel method
config.superpixel(2).superpixel_method = 'grayscale_AglMeanB';
% (b) Superpixel version to be used as basis
% % use superpixel segmentation parameters from previous step
% For grayscale-ladder .gs_l_T<f_threshold>_L<area_threshold>.<filter_version>_<mitochondria>
s = {};
for f = 0.1; %config.superpixel(1).save_f_thresholds
  s{end+1} = ...
    [ '.gs_amb_T', num2str(f,'%g'), '_', num2str(f-0.01,'%g'), ...
      '_b', num2str(0*(f>0),'%d'), ...
      '.BELc_5_mf7_e6_d4_m0_d5_a40', ...
      '.BELc_5_mf7_e6_d4']; %#ok<AGROW>
end
config.superpixel(2).superpixel_suffix = s; 
% () thresholds on the boundary fieldss
config.superpixel(2).f_thresholds = 0.005; %[0.01, 0.05, 0.1, 0.15, 0.2];
% () thresholds on the boundary field for which segmentations should be
% saved. Useful for reducing disk space.
config.superpixel(2).save_f_thresholds = config.superpixel(2).f_thresholds;
% Whether to save the segmentation overlaps as TIF files for viewing.
config.superpixel(2).save_segmentation_overlay = true;
% () minimum area of a segment - below this they merged with neighbors
config.superpixel(2).area_thresholds = 300;
% () Whether mitochondria are to be used to suppress false boundaries
config.superpixel(2).use_mitochondria = false;
% () The confidence threshold to be applied on the mitochondria
config.superpixel(2).mitochondria_confidence_threshold = 0.5; 
% () The thresholded mitochondria mask is eroded to ensure that correct
% boundaries are not obliterated
config.superpixel(2).mitochondria_erosion = 2; % 20
% * Minimum of a thresholded connected component for it to be considered
% mitochondria
config.superpixel(2).mitochondria_min_area = 40;
% () Whether to use vesicle detection to suppress false boundaries
config.superpixel(2).use_vesicle = false;
% () The threshold for the vesicle detection confidence
config.superpixel(2).vesicle_threshold = 0;
% () Radius of the vesicles. Boundaries within this distance from detected
% vesicle centers are obliterated.
config.superpixel(2).vesicle_dilation = 0;
% () Image filtering before performing segmentation (affects boundary
% detection).
config.superpixel(2).filter_version = 'BELc\_5\_mf7_e6_d4';
% () whether to display intermediate results
config.superpixel(2).is_verbose = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.3 Superpixel - agglomerative boundary vs. interior values
% Fisher Linear Discriminant
%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % % (a) Algorithm to be used - must match with the function called
% % % config.superpixel(3).method = 'grayscale_AglBIFL';
% % % % () Superpixel method
% % % config.superpixel(3).superpixel_method = 'grayscale_ladder';
% % % % (b) Superpixel version to be used as basis
% % % % % use superpixel segmentation parameters from previous step
% % % % For grayscale-ladder .gs_l_T<f_threshold>_L<area_threshold>.<filter_version>_<mitochondria>
% % % s = {};
% % % for f = 0.09; %config.superpixel(1).save_f_thresholds
% % %   s{end+1} = ...
% % %     [ '.gs_l_sp_T0.005_L240', ...
% % %       '.gs_amb_T', num2str(f,'%g'), '_', ...
% % %       num2str(f-0.01,'%g'), '_b', num2str(0*(f>0),'%d'), ...
% % %       '.BEL_m0_d5_a40', ...
% % %       '.BEL', ...
% % %       '.BEL']; %#ok<AGROW>
% % % end
% % % config.superpixel(3).superpixel_suffix = s; 
% % % % (c) thresholds on the boundary field
% % % config.superpixel(3).f_threshold_seq = 0.01;
% % % % () thresholds on the boundary field for which segmentations should be
% % % % saved. Useful for reducing disk space.
% % % config.superpixel(3).save_f_thresholds = 0.01;
% % % % Whether to save the segmentation overlaps as TIF files for viewing.
% % % config.superpixel(3).save_segmentation_overlay = true;
% % % % () minimum boundary length (pixel units) for two segments to be merged
% % % % should increase with increasing f_thresholds. E.g., 0 for f_thresholds
% % % % <=0.65 and 20 otherwise
% % % config.superpixel(3).length_threshold_seq = ...
% % %   20*(config.superpixel(3).f_threshold_seq>0);
% % % % () Maximum area of a segment, above this segments are not considered for
% % % % merging.
% % % config.superpixel(3).max_area_threshold = 2000;
% % % % () Whether mitochondria are to be excluded from image statistics
% % % config.superpixel(3).exclude_mitochondria_stat = true;
% % % % () Whether mitochondria are to be used to suppress false boundaries
% % % config.superpixel(3).use_mitochondria = false;
% % % % () The confidence threshold to be applied on the mitochondria
% % % config.superpixel(3).mitochondria_confidence_threshold = 0.0;
% % % % () The thresholded mitochondria mask is eroded to ensure that correct
% % % % boundaries are not obliterated
% % % config.superpixel(3).mitochondria_erosion = 20;
% % % % * Minimum of a thresholded connected component for it to be considered
% % % % mitochondria
% % % config.superpixel(3).mitochondria_min_area = 40;
% % % % () Whether to use vesicle detection to suppress false boundaries
% % % config.superpixel(3).use_vesicle = false;
% % % % () The threshold for the vesicle detection confidence
% % % config.superpixel(3).vesicle_threshold = 0;
% % % % () Radius of the vesicles. Boundaries within this distance from detected
% % % % vesicle centers are obliterated.
% % % config.superpixel(3).vesicle_dilation = 0;
% % % % () Image filtering before performing segmentation (affects boundary
% % % % detection).
% % % config.superpixel(3).filter_version = 'v0';
% % % % () whether to display intermediate results
% % % config.superpixel(3).is_verbose = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7 Choose a superpixel segmentation parameter for further processing
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Two options:
% --- (a) One parameter for all images ---
% Superpixel method
config.superpixel_choose.choice.method = 'grayscale_ladder'; %'grayscale_AglBIFL';
% use superpixel segmentation parameters from previous step
% E.g., for grayscale-ladder it would be of the form
% .gs_l_T<f_threshold>_L<area_threshold>.<filter_version>_<mitochondria m<threshold_d<amount of erosion>>  
config.superpixel_choose.choice.seg_suffix = ...
    [ '.gs_l_sp_T0.005_L300', ...
      '.gs_amb_T0.1_0.09_b0', ...
      '.BELc_5_mf7_e6_d4_m0_d5_a40', ...
      '.BELc_5_mf7_e6_d4', ...
      '.BELc_5_mf7_e6_d4'];
% % % config.superpixel_choose.choice.seg_suffix = ...
% % %     [ '.gs_abif_sp_T0.01_0_b20', ...
% % %       '.gs_l_sp_T0.005_L240', ...
% % %       '.gs_amb_T0.09_0.08_b0', ...
% % %       '.BEL_m0_d5_a40', ...
% % %       '.BEL', ...
% % %       '.BEL', ...
% % %       '.v0'];
% --- (b) For each image choose a parameter from a set ---
% Use a GUI to choose between a set of parameters for each image.
% config.superpixel_choose.choice.method = '#(SP_CHOICE)';
% config.superpixel_choose.choice.seg_suffix = '#(SP_CHOICE)';
% % * This is a list of superpixel parameters. Each element is a struct with
% % fields:
% %   .method       the segmentation method, e.g., 'grayscale_ladder'.
% %   .seg_suffix   parameters for segmentation, similar to those used in
% %                   superpixel_suffix in reconstruction scripts.
% config.superpixel_choose.param(1).method = 'grayscale_AglBIFL';
% config.superpixel_choose.param(1).seg_suffix = ...
%   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.18_0.17_b0.gs_amb_T0.18_0.17_b0.BEL_e4_d4.BEL_e4_d4.v0';
% 
% config.superpixel_choose.param(2).method = 'grayscale_AglBIFL';
% config.superpixel_choose.param(2).seg_suffix = ...
%   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.18_0.17_b0.gs_amb_T0.18_0.17_b0.BEL_e4_d4.BEL_e4_d4.v0';
% 
% config.superpixel_choose.param(3).method = 'grayscale_AglBIFL';
% config.superpixel_choose.param(3).seg_suffix = ...
%   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.18_0.17_b0.gs_amb_T0.18_0.17_b0.BEL_e4_d4.BEL_e4_d4.v0';
% 
% config.superpixel_choose.param(4).method = 'grayscale_AglBIFL';
% config.superpixel_choose.param(4).seg_suffix = ...
%   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.18_0.17_b0.gs_amb_T0.18_0.17_b0.BEL_e4_d4.BEL_e4_d4.v0';
% % * Desired decision tree: This specifies the manner in which choices are
% % to be presented to the user. Two options:
% % (1) use one of the predefined decision trees (see get_basic_config.m), OR
% % (2) define a custom decision tree: See get_basic_config, Sec. 12 for examples.
% % config.superpixel_choose.decision_tree = config.decision_tree_predef_binary_4_choice;
% config.superpixel_choose.decision_tree = config.decision_tree_predef_binary_4_choice_bias_4;
% % Whether to display the image after histogram equalization
% config.superpixel_choose.is_image_histeq = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8 Multistage superpixel to segment computation
%%%%%%%%%%%%%%%%%%%%%%%%%%
config.superpixel_2_seg = repmat(config.superpixel_2_seg, [2 1]);
%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.1 Superpixel to segment - agglomerative mean boundary
%%%%%%%%%%%%%%%%%%%%%%%%%%
% (a) Algorithm to be used - must match with the function called
config.superpixel_2_seg(1).method = 'grayscale_AglMeanB';
% Superpixel method: For the 1st stage in superpixel_2_seg this should
% always be config.superpixel_choose.choice.method. So leave the following
% empty ('') for the 1st stage.
config.superpixel_2_seg(1).superpixel_method = '';
% (b) Superpixel version to be used as basis. For the 1st stage in
% superpixel_2_seg this should always be
% config.superpixel_choose.choice.method. So leave the following empty ('')
% for the 1st stage. 
config.superpixel_2_seg(1).superpixel_suffix = '';
% (c) thresholds on the boundary field
config.superpixel_2_seg(1).f_threshold_seq = 0.01:0.01:0.2;
% () thresholds on the boundary field for which segmentations should be
% saved. Useful for reducing disk space.
config.superpixel_2_seg(1).save_f_thresholds = 0.06:0.02:0.2; %0.06:0.02:0.2;
% () minimum boundary length (pixel units) for two segments to be merged
% should increase with increasing f_thresholds. E.g., 0 for f_thresholds
% <=0.65 and 20 otherwise
config.superpixel_2_seg(1).length_threshold_seq = ...
  0*(config.superpixel_2_seg(1).f_threshold_seq>0.65);
% () Whether mitochondria are to be used to suppress false boundaries
config.superpixel_2_seg(1).use_mitochondria = false;
% () The confidence threshold to be applied on the mitochondria
config.superpixel_2_seg(1).mitochondria_confidence_threshold = 0.0;
% () The thresholded mitochondria mask is eroded to ensure that correct
% boundaries are not obliterated
config.superpixel_2_seg(1).mitochondria_erosion = 20;
% () Whether to use vesicle detection to suppress false boundaries
config.superpixel_2_seg(1).use_vesicle = false;
% () The threshold for the vesicle detection confidence
config.superpixel_2_seg(1).vesicle_threshold = 0;
% () Radius of the vesicles. Boundaries within this distance from detected
% vesicle centers are obliterated.
config.superpixel_2_seg(1).vesicle_dilation = 0;
% () Image filtering before performing segmentation (affects boundary
% detection).
config.superpixel_2_seg(1).filter_version = 'BELc\_5\_mf7_e2_d4';
% () whether to display intermediate results
config.superpixel_2_seg(1).is_verbose = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.2 Superpixel to segment - agglomerative median boundary
%%%%%%%%%%%%%%%%%%%%%%%%%%
% (a) Algorithm to be used - must match with the function called
config.superpixel_2_seg(2).method = 'grayscale_AglMedianB';
% () Superpixel method
config.superpixel_2_seg(2).superpixel_method = 'grayscale_AglMeanB';
% (b) Superpixel version to be used as basis
% % use superpixel segmentation parameters from previous step
% For grayscale-ladder .gs_l_T<f_threshold>_L<area_threshold>.<filter_version>_<mitochondria m<threshold_d<amount of erosion>>
config.superpixel_2_seg(2).superpixel_suffix = ...
  [ '.gs_amb_sp_T0.18_0.17_b0', ...
    config.superpixel_choose.choice.seg_suffix, ...
    '.BEL_mf11'];
% '.gs_l_T0.47_L100.v_heq_mf3_e2_m0_d2_a500'; 
% (c) thresholds on the boundary field
config.superpixel_2_seg(2).f_threshold_seq = 0.01:0.01:0.18;
% () thresholds on the boundary field for which segmentations should be
% saved. Useful for reducing disk space.
config.superpixel_2_seg(2).save_f_thresholds = 0.18;
% () minimum boundary length (pixel units) for two segments to be merged
% should increase with increasing f_thresholds. E.g., 0 for f_thresholds
% <=0.65 and 20 otherwise
config.superpixel_2_seg(2).length_threshold_seq = ...
  0*(config.superpixel_2_seg(2).f_threshold_seq>0.65);
% () Whether mitochondria are to be used to suppress false boundaries
config.superpixel_2_seg(2).use_mitochondria = false;
% () The confidence threshold to be applied on the mitochondria
config.superpixel_2_seg(2).mitochondria_confidence_threshold = 0.0;
% () The thresholded mitochondria mask is eroded to ensure that correct
% boundaries are not obliterated
config.superpixel_2_seg(2).mitochondria_erosion = 20;
% () Whether to use vesicle detection to suppress false boundaries
config.superpixel_2_seg(2).use_vesicle = false;
% () The threshold for the vesicle detection confidence
config.superpixel_2_seg(2).vesicle_threshold = 0;
% () Radius of the vesicles. Boundaries within this distance from detected
% vesicle centers are obliterated.
config.superpixel_2_seg(2).vesicle_dilation = 0;
% () Image filtering before performing segmentation (affects boundary
% detection).
config.superpixel_2_seg(2).filter_version = 'BEL_mf11';
% Boundary version
config.superpixel_2_seg(2).boundary_version = 'c_5_mf7';
% () whether to display intermediate results
config.superpixel_2_seg(2).is_verbose = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.3 Superpixel to segment - agglomerative boundary vs. interior values
% Fisher Linear Discriminant
%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % % (a) Algorithm to be used - must match with the function called
% % % config.superpixel_2_seg(3).method = 'grayscale_AglBIFL';
% % % % () Superpixel method
% % % config.superpixel_2_seg(3).superpixel_method = 'grayscale_AglMedianB';
% % % % (b) Superpixel version to be used as basis
% % % % % use superpixel segmentation parameters from previous step
% % % % For grayscale-ladder .gs_l_T<f_threshold>_L<area_threshold>.<filter_version>_<mitochondria>
% % % config.superpixel_2_seg(3).superpixel_suffix = ...
% % %   [ '.gs_amdb_sp_T0.18_0.17_b0', ...
% % %     '.gs_amb_sp_T0.18_0.17_b0', ...
% % %     config.superpixel_choose.choice.seg_suffix, ...
% % %     '.BEL_mf11', ...
% % %     '.BEL_mf11'];
% % % % (c) thresholds on the boundary field
% % % config.superpixel_2_seg(3).f_threshold_seq = 0.01:0.01:0.2;
% % % % () thresholds on the boundary field for which segmentations should be
% % % % saved. Useful for reducing disk space.
% % % config.superpixel_2_seg(3).save_f_thresholds = 0.26; %0.36:0.04:0.6;
% % % % Whether to save the segmentation overlaps as TIF files for viewing.
% % % config.superpixel_2_seg(3).save_segmentation_overlay = true;
% % % % () minimum boundary length (pixel units) for two segments to be merged
% % % % should increase with increasing f_thresholds. E.g., 0 for f_thresholds
% % % % <=0.65 and 20 otherwise
% % % config.superpixel_2_seg(3).length_threshold_seq = ...
% % %   20*(config.superpixel_2_seg(3).f_threshold_seq>0);
% % % % () Maximum area of a segment, above this segments are not considered for
% % % % merging.
% % % config.superpixel_2_seg(3).max_area_threshold = 2000;
% % % % () Whether mitochondria are to be excluded from image statistics
% % % config.superpixel_2_seg(3).exclude_mitochondria_stat = true;
% % % % () Whether mitochondria are to be used to suppress false boundaries
% % % config.superpixel_2_seg(3).use_mitochondria = false;
% % % % () The confidence threshold to be applied on the mitochondria
% % % config.superpixel_2_seg(3).mitochondria_confidence_threshold = 0.0;
% % % % () The thresholded mitochondria mask is eroded to ensure that correct
% % % % boundaries are not obliterated
% % % config.superpixel_2_seg(3).mitochondria_erosion = 20;
% % % % * Minimum of a thresholded connected component for it to be considered
% % % % mitochondria
% % % config.superpixel_2_seg(3).mitochondria_min_area = 40;
% % % % () Whether to use vesicle detection to suppress false boundaries
% % % config.superpixel_2_seg(3).use_vesicle = false;
% % % % () The threshold for the vesicle detection confidence
% % % config.superpixel_2_seg(3).vesicle_threshold = 0;
% % % % () Radius of the vesicles. Boundaries within this distance from detected
% % % % vesicle centers are obliterated.
% % % config.superpixel_2_seg(3).vesicle_dilation = 0;
% % % % () Image filtering before performing segmentation (affects boundary
% % % % detection).
% % % config.superpixel_2_seg(3).filter_version = 'v0';
% % % % () whether to display intermediate results
% % % config.superpixel_2_seg(3).is_verbose = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9 Choose a segmentation parameter for further processing
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Two options:
% --- (a) One parameter for all images ---
% Segmentation method
config.segmentation_choose.choice.method = 'grayscale_AglMedianB'; %'grayscale_AglBIFL';
% use segmentation parameters from previous step
% E.g., for grayscale-ladder it would be of the form
% .gs_l_T<f_threshold>_L<area_threshold>.<filter_version>_<mitochondria m<threshold_d<amount of erosion>>  
config.segmentation_choose.choice.seg_suffix = ...
  [ '.gs_amdb_sp_T0.18_0.17_b0', ...
    '.gs_amb_sp_T0.18_0.17_b0', ...
    config.superpixel_choose.choice.seg_suffix, ...
    '.BEL_mf11', ...
    '.BEL_mf11'];
% % % config.segmentation_choose.choice.seg_suffix = ...
% % %   [ '.gs_abif_sp_T0.26_0.25_b20', ...
% % %     '.gs_amdb_sp_T0.18_0.17_b0', ...
% % %     '.gs_amb_sp_T0.18_0.17_b0', ...
% % %     config.superpixel_choose.choice.seg_suffix, ...
% % %     '.BEL_e4_d4', ...
% % %     '.BEL_e4_d4', ...
% % %     '.v0'];
% --- (b) For each image choose a parameter from a set ---
% Use a GUI to choose between a set of parameters for each image.
% % config.segmentation_choose.choice.method = '#(SEG_CHOICE)';
% % config.segmentation_choose.choice.seg_suffix = '#(SEG_CHOICE)';
% % % * This is a list of superpixel parameters. Each element is a struct with
% % % fields:
% % %   .method       the segmentation method, e.g., 'grayscale_ladder'.
% % %   .seg_suffix   parameters for segmentation, similar to those used in
% % %                   superpixel_suffix in reconstruction scripts.
% % config.segmentation_choose.param(1).method = 'grayscale_AglBIFL';
% % config.segmentation_choose.param(1).seg_suffix = ...
% %   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.18_0.17_b0.gs_amb_T0.18_0.17_b0.BEL_e4_d4.BEL_e4_d4.v0';
% % 
% % config.segmentation_choose.param(2).method = 'grayscale_AglBIFL';
% % config.segmentation_choose.param(2).seg_suffix = ...
% %   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.26_0.25_b0.gs_amb_T0.26_0.25_b0.BEL_e4_d4.BEL_e4_d4.v0';
% % 
% % config.segmentation_choose.param(3).method = 'grayscale_AglBIFL';
% % config.segmentation_choose.param(3).seg_suffix = ...
% %   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.18_0.17_b0.gs_amb_T0.18_0.17_b0.BEL_e4_d4.BEL_e4_d4.v0';
% % 
% % config.segmentation_choose.param(4).method = 'grayscale_AglBIFL';
% % config.segmentation_choose.param(4).seg_suffix = ...
% %   '.gs_abif_sp_T0.26_0.25_b20.gs_amdb_sp_T0.18_0.17_b0.gs_amb_T0.18_0.17_b0.BEL_e4_d4.BEL_e4_d4.v0';
% * Desired decision tree: This specifies the manner in which choices are
% to be presented to the user. Two options:
% (1) use one of the predefined decision trees (see get_basic_config.m), OR
% (2) define a custom decision tree: See get_basic_config, Sec. 12 for examples.
config.superpixel_choose.decision_tree = config.decision_tree_predef_binary_4_choice;
% Whether to display the image after histogram equalization
config.segmentation_choose.is_image_histeq = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 11. 3D linkage graph - training
%%%%%%%%%%%%%%%%%%%%%%%%%%
% () Stack resolution parameters for area and boundary histograms. These
% are to be used if the models having been trained on images of resolution
% different from the current one. This might improve the results,
% especially during boot-strapping.
config.stack.length_factor = 1;
config.stack.area_factor = config.stack.length_factor.^2;
% (a) 3D annotation file to be used for training
config.linkage.train.manual_annotation_file = ...
  '/groups/chklovskii/chklovskiilab/electron_microscopy_data/medulla.HPF.Leginon.3500x.zhiyuan.fall2008/manual_annotations/proofread.shinya.12152008.wcat.mat';
% (b) Version name for the linkage model
config.linkage.train.save_suffix = '.02022009';
% (c) Type for feature to be used for linkage
config.linkage.feature.type = 'intensity_pair_hist_v2c';
% (d) For intensity pair hist., the intensity bins in the histograms
config.linkage.feature.intensity_bins = 0:0.1:1.0;
% (e) Type of classifier
config.linkage.model.type = 'boost';
% (f) Number of iterations of boosting
config.linkage.model.n_iteration = 40;
% (g) Tree depth
config.linkage.model.tree_depth = 2;
% (h) The model version to be used when applying the linkage on the stack
config.linkage.apply.model_suffix = '.02022009';
% (j) Verbose
config.linkage.apply.is_verbose = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12. Dump to proofreader
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Which proofreader is being used. E.g., matlab_gui, Raveler.
config.proofreader.method = 'Raveler'; %'matlab_gui';

%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14. Evaluate segment boundaries
%%%%%%%%%%%%%%%%%%%%%%%%%%
% () Ground truth segmentation to be used for evaluation.
% Format similar to that for volume reconstruction evaluation.
% seg is a 3D matrix of body labels for the voxels.
config.eval_segment_boundary.groundtruth_file = ...
  'proofread.a.007.011.katerina.03192009.seg.mat';
% () Lsit of segments in the ground-truth that may be over-segmented without
% penalty. Use this for irrelevant structurs , e.g., glia.
config.eval_segment_boundary.oversegment_ignore_groundtruth_label = [];
% () Method to be evaluated
% and Parameters for the method to be evaluated in the segmentation suffix
% There are two options:
% * Verbose suffix to provide a specific combination of parameters
% config.eval_segment_boundary.seg_suffix = '.gs_l_sp_T0.5_L200.gs_l_T0.44_L200.v_heq_mf5_e1_m0_d2_a500.v_heq_mf3_e2';
% * Or, specify a list of parameters. In this case, provide the ANSI C
% compatible printf format string and the list of ids to be printed with
% the format string.
% For evaluating grayscale agglo. mean boundary superpixel-to-segment
config.eval_segment_boundary.method = 'grayscale_AglMeanB';
config.eval_segment_boundary.seg_suffix_format = ...
  [ '.gs_amb_sp%s', ...
    config.superpixel_choose.choice.seg_suffix, ...
    '.BELc_5_mf7_e2_d4'];
s = {};
for f = config.superpixel_2_seg(1).save_f_thresholds
    s{end+1} = [ '_T', num2str(f), '_', num2str(f-0.01), '_b', ...
      num2str(0*(f>0.65))]; %#ok<AGROW>
end
config.eval_segment_boundary.seg_suffix_id = s;
% Parameters used for reconstructing the ministack - for baseline.
% () Maximum matching distance between ground-truth and automatic
% segmentation boundaries. Depends upon resolution. Keeping it very small
% results in zig-zag boundaries being penalized even though the overall
% segmentation is OK.
config.eval_segment_boundary.max_match_dist = 15;
% () Verbose
config.eval_segment_boundary.is_verbose = true;
% () Mode of evaluation: 'pr-curve', 'match-save'
% pr-curve: A precision-recall is generated, typically for a range of
% segmenation parameters specified using seg_suffix_id.
% match-save: Saves the bipartite matching results to .tif images and
% creates a .tex file. Compiling this file using pdflatex produces a pdf of
% the matching results. This enables detailed inspection of the evaluation.
config.eval_segment_boundary.mode = 'pr-curve';
% config.eval_segment_boundary.mode = 'match-save';
% type of marker for the pr-curve
config.eval_segment_boundary.marker = 'd-';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% III. Reconstruction Routines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pipeline(module_id, config);

return

end