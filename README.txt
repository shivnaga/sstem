FlyEM Segmentation Pipeline Code

The results of this pipeline included labeled SSTEM images that were used
as the starting point for a series of manual proofreading steps.  The pipeline
was applied at various points of the reconstruction process, sometimes after
improvements in the alignment algorithms and expansion of the regions of interest
within the acquired image volume.

We have also included a draft paper addressing this technique in more detail.
Please see "Segmentation of Neurons in Electron Micrographs" (em_2d_segmentation.pdf).

Use the following steps to get started:
- Launch MATLAB ; remaining steps are in MATLAB

>> startup.m 

>> cd reconstructions/scripts/medulla_Leginon

% config file for running the EM reconstruction pipeline.
>> edit quick_test_0241_0244.m

% runs the 2D superpixel segmentation module.
>> em_reconstruct('quick_test_0241_0244.m', [5], 'is_verbose', true, 'is_verbose_figures', true);
 