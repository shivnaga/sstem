#!/bin/env python
"""
This script takes several annotation files and a list of
T-bar identifiers to be given to different proofreaders.
It then generates valid synaptic annotation files for each
proofreader.  The script is derived from Don's original
format-synapses-shinya.py.

for usage, do: python (scriptname) -h   (or --help) 

WTK - 10/5/2010
"""



# ------------------------- imports -------------------------
# std lib:
import optparse
import os
import StringIO
import sys


# this is far too fancy, but it will work on Raveler machines with
#   Python 2.5:
try:
    import json
except ImportError:
    try:
        import simplejson as json
    except ImportError:
        # try to find it on Raveler machines:
        sys.path.append("/usr/local/raveler-tiled")
        try:
            import simplejson as json
        except ImportError:
            print "can't find a JSON library!"
            sys.exit(1)


# ------------------------- constants -------------------------

__version__ = "0.1"

# dictionary for body names:
bodynamesdict = {}

# ------------------------- assign_synapses() -------------------------
def assign_synapses(jsondata):
    """
    traverse the JSON data and partition synapses by pre-synaptic 
    body
    
    input: JSON data
    output: dict: {body ID: [list of synapses, JSON format]}
    """
    
    output = {}
    
    for synapse in jsondata:
        bodyid = synapse["T-bar"]["body ID"]
        if synapse["T-bar"]["status"] == 'final':  # Reset status
            synapse["T-bar"]["status"] = 'working'
        if bodyid in output:
            output[bodyid].append(synapse)
        else:
            output[bodyid] = [synapse]
    
    return output
    
    # end assign_synapses()

# ------------------------- bodyname() -------------------------
def bodyname(bodyid):
    """
    returns the name corresponding to a body ID, if one exists;
    otherwise, just returns the input
    """
    
    return bodynamesdict.get(bodyid, bodyid)
    
    # end bodyname()


def loadnames(filename):
    """
    load a file with bodyID to biological name mapping; assume it's
    in simple two column format, skip blanks and lines with '#';
    data is stored in script-level global
    """
    
    data = open(filename, 'rt').readlines()
    
    for line in data:
        line = line.strip()
        if line and not line.startswith('#'):
            bodyid, rest = line.split(None, 1)
            bodynamesdict[int(bodyid)] = rest


def read_annotations(filepath):
    """
    read in JSON annotation file
    
    input: filepath
    output: dictionary from file
    """
    
    data = json.loads(open(filepath).read())
    
    if data["metadata"]["description"] != "synapse annotations":
        print "this doesn't seem to be a synapse annotation file!"
        sys.exit(1)
    
    return data


def read_idsplits(filename):
    """
    read in ids of T-bars for each proofreader in csv format
    """
    assigned_ids = {}
    data = open(filename, 'rt').readlines()
    for line in data:
        line = line.strip()
        ids = line.split(',')
        for (proofreader, id) in enumerate(ids):
            num_id = int(id)
            if num_id != 0:
                if proofreader in assigned_ids:
                    assigned_ids[proofreader].append(num_id)
                else:
                    assigned_ids[proofreader] = [num_id]
    return assigned_ids

def get_synapses_by_arjun_ids(synapses_in_bodies, zmin, zmax):
    """
    return dictionary of synapses by arjun ids in his excel spreadsheet
    """
    arjun_id = 0
    synapses_by_arjun_ids = {}
    for bodyid in sorted(synapses_in_bodies.keys()):
        synapselist = synapses_in_bodies[bodyid]
    
        # partition synapses by partner and plane:
        partnercount = {}
        planepartners = {}
        for synapse in synapselist:
            x, y, z = synapse["T-bar"]["location"] 
            if z not in planepartners:
                planepartners[z] = []
            if "partners" in synapse:
                planepartners[z].append([x, synapse])
        
        # loop over planes, list all; first list should be in-line with z:
        for z in sorted(planepartners.keys()):
            if int(z) >= zmin and int(z) <= zmax:
                looplist = list(sorted(planepartners[z], key=lambda item: item[0]))
                if looplist:
                    for x, synapse in looplist:
                        filtered_synapse = {}
                        filtered_synapse["T-bar"] = synapse["T-bar"]
                        if "partners" in synapse:
                            filtered_synapse["partners"] = []
                            for partner in synapse["partners"]:
                                x, y, z = partner["location"]
                                if z >= zmin and z <= zmax:
                                    filtered_synapse["partners"].append(partner)
                        arjun_id += 1
                        synapses_by_arjun_ids[arjun_id] = filtered_synapse
    return synapses_by_arjun_ids

def write_annotation_file(f, assigned_ids, synapses_by_arjun_ids, metadata):
    """
    Write annotation file for all synapses with the assigned_ids.
    """
    json_data["data"] = []
    json_data["metadata"] = metadata
    
    for id in assigned_ids:
        json_data["data"].append(synapses_by_arjun_ids[id])
    
    json.dump({"metadata": metadata, "data": data}, f, indent=4)

# ------------------------- script starts here -------------------------
if __name__ == "__main__":
    
    # use optparse module for command-line options:
    usage = "usage: %prog annotationfile idsplits [options]"
    parser = optparse.OptionParser(usage=usage, version="%%prog %s" % __version__)
    parser.add_option("--names", dest="namefile",
            help="file with biological names")    
    parser.add_option("--out", dest="outputfile",
            default="annotations-synapse-proofreader", help="base filenames for generated synaptic annotation files")
    parser.add_option("--range", dest="slices",
            default="311:460", help="range of slices to use in format 'zmin:zmax'")
    (options, args) = parser.parse_args()    
    
    if len(args) < 2:
        parser.print_help()
        sys.exit(2)

    infilename = args[0]
    idsplits_filename = args[1]
    
    zminstr, zmaxstr = options.slices.split(":")
    zmin = int(zminstr)
    zmax = int(zmaxstr)
    
    if not os.path.exists(infilename):
        print "Annotation file %s doesn't seem to exist!" % infilename
        sys.exit(1)
    annotations = read_annotations(infilename)
    
    if not os.path.exists(idsplits_filename):
        print "File with T-bar assignments %s doesn't seem to exist!" % idsplits_filename
        sys.exit(1)
    assigned_ids = read_idsplits(idsplits_filename)
    
    if options.namefile is not None:
        if not os.path.exists(options.namefile):
            print "name file %s doesn't seem to exist; continuing..." % options.namefile
        else:
            loadnames(options.namefile)

    out_filenames = {}
    for proofreader in assigned_ids:
        out_filenames[proofreader] = "%s-%s.json" % (options.outputfile, proofreader+1)
    print("Creating %d proofreader synapse annotation files for slices %d to %d." % \
          (len(assigned_ids), zmin, zmax))

    synapses_in_bodies = assign_synapses(annotations["data"])
    synapses_by_arjun_ids = get_synapses_by_arjun_ids(synapses_in_bodies, zmin, zmax)

    metadata = annotations["metadata"]
    metadata["software"] = "create_randomized_annotations.py (WTK)"
    metadata["software version"] = "0.1"
    
    for proofreader in assigned_ids:
        with open(out_filenames[proofreader], 'w') as f:
            print("Creating proofreader synapse annotation file: %s" % (out_filenames[proofreader]))
            data = []
            for id in assigned_ids[proofreader]:
                data.append(synapses_by_arjun_ids[id])
            json.dump({"metadata": metadata, "data": data}, f, indent=2)
