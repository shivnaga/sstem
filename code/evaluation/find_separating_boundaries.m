function boundary_map = find_separating_boundaries(segmentation, ...
    body_pairs_list, output_format, reference_CC)
% BOUNDARY_MAP = FIND_SEPARATING_BOUNDARIES(SEGMENTATION, BODY_PAIRS_LIST, ...
%     OUTPUT_FORMAT, REFERENCE_CC)
% For an n-by-2 list of body labels, find boundaries separating segments 
% BODY_PAIRS_LIST(:,1) and BODY_PAIRS_LIST(:,2) in SEGMENTATION, intersected
% with BODY_PAIRS_LIST(:,3) in REFERENCE_CC (a connected components object) if
% the latter is provided.
% If specified, OUTPUT_FORMAT must be one of the following: binary (default), 
% decreasing.

    if nargin < 4 || ~isstruct(reference_CC),
        reference_CC = bwconncomp(ones(size(segmentation)));
        body_pairs_list(:,3) = 1;
    end
    
    if nargin < 3,
        output_format = 'binary';
    end
    
    s = size(segmentation);

    if isempty(body_pairs_list),
        if strcmp(output_format, 'binary'),
            boundary_map = zeros(s, 'uint8');
        else
            boundary_map = zeros(s, 'uint16');
        end
        return;
    end
    
    body_pairs_list = unique([min(body_pairs_list(:,1:2), [], 2) ...
                              max(body_pairs_list(:,1:2), [], 2) ...
                              body_pairs_list(:,3)], 'rows');
    n = size(body_pairs_list, 1);
    max_value = intmax('uint16');
    
    
    if strcmp(output_format, 'binary'),
        boundary_map = zeros(s, 'uint8');
        boundaries = sparse(body_pairs_list(:,1), body_pairs_list(:,2), ones(n,1));
        get_boundary_value = @get_boundary_value_1;
    elseif strcmp(output_format, 'decreasing'),
        boundary_map = zeros(s, 'uint16');
        m = double(intmax('uint16'));
        boundaries = sparse(body_pairs_list(:,1), body_pairs_list(:,2), m:-1:(m-n+1));
        get_boundary_value = @get_boundary_value_decreasing;
    end

    % TO CLEAN UP: HACK TO LIMIT TIME SPENT
    N = numel(unique(body_pairs_list(:)));
    t_labels = 0.56*n + 0.34*N + 1.5;
    t_label_sets = 0.004*n + 1.319*N + 4.5;
    if (t_labels > 5400) && (t_label_sets > 5400),
        fprintf('ignored threshold.\n');
        boundary_map(:) = intmax('uint16')/2;
        return;
    elseif t_labels < t_label_sets,
        scan_method = 'labels';
    else % t_label_sets < t_labels
        scan_method = 'label-sets';
    end
    
    h = waitbar(0, 'running find\_separating\_boundaries...');
    if strcmp(scan_method, 'linear'),
        zero_voxels = myind2sub(size(segmentation), find(segmentation == 0));
        for v=1:size(zero_voxels,1),
            labels = unique(segmentation(get_neighbouring_indices(zero_voxels(v,:), s)));
            for i=1:(numel(labels)-1),
                for j=(i+1):numel(labels),
                    boundary_map(sub2ind(s, zero_voxels(v,1), zero_voxels(v,2), ...
                                        zero_voxels(v,3))) = full(boundaries(i,j));
                end
            end
            waitbar(v/size(zero_voxels,1), h);
        end
    elseif strcmp(scan_method, 'labels'),
        se = diamond_se(3,3);
        boundary_high_labels = double(imdilate(segmentation, se)) .* double(segmentation == 0);
        segmentation_mb =  segmentation;
        segmentation_mb(segmentation == 0) = realmax;
        boundary_low_labels = double(imerode(segmentation_mb, se)) .* double(segmentation == 0);
        for i=1:n,
            l1 = body_pairs_list(i, 1);
            l2 = body_pairs_list(i, 2);
            lr = body_pairs_list(i, 3);
            boundary = intersect(find((boundary_low_labels == l1) .* (boundary_high_labels == l2)), ...
                reference_CC.PixelIdxList{lr});
            boundary_map(boundary) = get_boundary_value(max_value, i);
            waitbar(i/n, h);
        end
    elseif strcmp(scan_method, 'label-sets'),
        se = diamond_se(3,3);
        boundary_high_labels = double(imdilate(segmentation, se)) .* double(segmentation == 0);
        segmentation_mb =  segmentation;
        segmentation_mb(segmentation == 0) = realmax;
        boundary_low_labels = double(imerode(segmentation_mb, se)) .* double(segmentation == 0);
        ids = cell(1,max(body_pairs_list(:)));
        unique_label_list = unique(body_pairs_list(:))';
        for l=unique_label_list,
            ids{l} = union(find(boundary_high_labels == l), find(boundary_low_labels == l));
            waitbar(find(l==unique_label_list)/numel(unique_label_list), h);
        end
        for i=1:n,
            l1 = body_pairs_list(i,1);
            l2 = body_pairs_list(i,2);
            lr = body_pairs_list(i,3);
            boundary = intersect(intersect(ids{l1}, ids{l2}), ...
                reference_CC.PixelIdxList{lr});
            boundary_map(boundary) = get_boundary_value(max_value, i);
            waitbar(i/n, h);
        end
    end
    delete(h);
end

function value = get_boundary_value_1(~,~)
    value = 1;
end

function value = get_boundary_value_decreasing(max_value, i)
    value = max_value - i + 1;
end
