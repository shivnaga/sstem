#!/bin/env python
"""
This script takes a body annotations file, checks for
corrected bodies, and then addes a comment for the body
to designate it as an "anchor body"

for usage, do: python (scriptname) -h   (or --help) 

WTK - 10/12/2010
"""



# ------------------------- imports -------------------------
# std lib:
import optparse
import os
import StringIO
import sys


# this is far too fancy, but it will work on Raveler machines with
#   Python 2.5:
try:
    import json
except ImportError:
    try:
        import simplejson as json
    except ImportError:
        # try to find it on Raveler machines:
        sys.path.append("/usr/local/raveler-tiled")
        try:
            import simplejson as json
        except ImportError:
            print "can't find a JSON library!"
            sys.exit(1)


# ------------------------- constants -------------------------

__version__ = "0.1"


def read_annotations(filepath):
    """
    read in JSON annotation file
    
    input: filepath
    output: dictionary from file
    """
    
    data = json.loads(open(filepath).read())
    
    if data["metadata"]["description"] != "body annotations":
        print "this doesn't seem to be a body annotation file!"
        sys.exit(1)
    
    return data


# ------------------------- script starts here -------------------------
if __name__ == "__main__":
    
    # use optparse module for command-line options:
    usage = "usage: %prog annotationfile [options]"
    parser = optparse.OptionParser(usage=usage, version="%%prog %s" % __version__)
    parser.add_option("--out", dest="outputfile",
            default="stdout", help="base filenames for generated body annotation file")
    (options, args) = parser.parse_args()    
    
    if len(args) != 1:
        parser.print_help()
        sys.exit(2)
    infilename = args[0]
    
    if not os.path.exists(infilename):
        print "Annotation file %s doesn't seem to exist!" % infilename
        sys.exit(1)
    annotations = read_annotations(infilename)
    
    if options.outputfile == "stdout":
        # output to screen
        f = StringIO.StringIO()
    else:
        # user provided an output path:
        f = open(options.outputfile, 'wt')
    
    for body in annotations["data"]:
        if "status" in body and body["status"] == "corrected":
            body["comment"] = "anchor body"
    json.dump(annotations, f, indent=4)
    
    if options.outputfile == "stdout":
        print f.getvalue()
    
    f.close()

