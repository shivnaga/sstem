function keys = key64_from_tuple(tuple_keys)
% supports tuple values up to 2^20 ~ 10^6
    keys = uint64(2^20*tuple_keys(:,1) + tuple_keys(:,2));
end