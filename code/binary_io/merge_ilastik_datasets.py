#!/usr/bin/env python

import os, sys
import shutil
import argparse
import h5py
from numpy import array, dot, shape, reshape

class H5pyOpenRead(argparse.Action):
    def __call__(self, parser, namespace, value, option):
        setattr(namespace, self.dest, h5py.File(value, 'r'))

class H5pyOpenWrite(argparse.Action):
    def __call__(self, parser, namespace, value, option):
        setattr(namespace, self.dest, h5py.File(value, 'w'))

class EvalString(argparse.Action):
    def __call__(self, parser, namespace, value, option):
        setattr(namespace, self.dest, eval(value))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Copy first ilastik file but use data (by default, labels) from second.'
    )
    parser.add_argument('src1', help='the main source file')
    parser.add_argument('src2', action=H5pyOpenRead,
        help='the secondary source file'
    )
    parser.add_argument('dst', help='the destination source file')
    parser.add_argument('-g', '--group', 
        default='DataSets/dataItem00/labels/data',
        metavar='STR',
        help='the path to copy from the secondary source file'
    )
    parser.add_argument('-s', '--reshape', action='store_true',
        help='reshape the src2 data to add a trailing singleton dimension'
    )
    parser.add_argument('-S', '--specify-shape', action=EvalString,
        metavar='TUPLE',
        help='give a specific shape for the src2 data'
    )

    args = parser.parse_args()

    shutil.copyfile(args.src1, args.dst)
    dst = h5py.File(args.dst, 'a')
    del dst[args.group] # remove existing group from src1
    group_to_copy = args.src2[args.group]
    if args.reshape:
        dst[args.group] = reshape(group_to_copy, list(shape(group_to_copy))+[1])
    elif hasattr(args, 'specify_shape'):
        dst[args.group] = reshape(group_to_copy, args.specify_shape)
    else:
        dst[args.group] = group_to_copy

    dst.close()
