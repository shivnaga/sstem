#!/bin/bash

# You ONLY need to run this file if you have not put the below definitions in
# your ~/.bashrc file. Remember to replace "$PWD" with the correct directories

# These definitions are required to run the matlab compiler in the cluster
export MCR_INHIBIT_CTF_LOCK=1
export PATH=$PATH:/usr/local/matlab/bin
export KMP_DUPLICATE_LIB_OK=true

# These definitions are required by various parts of the EM reconstruction
# pipeline
export EMROOT=$PWD
export EM_CODE_DIR=$EMROOT/code
export CODE_DIR=$EM_CODE_DIR
export EM_BIN_DIR=$EMROOT/bin
export EMBIN=$EM_BIN_DIR
export PYTHONPATH=$PYTHONPATH:$EM_CODE_DIR/lib

# Add this line to put the EM reconstruction binaries in your path
export PATH=$PATH:$EM_BIN_DIR
