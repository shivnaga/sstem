datadir = '/opt/local/br/';

bm = readVTK('/groups/chklovskii/home/nuneziglesiasj/Projects/em_denoising/data/10x10x10_cropped/vtk_format/boundary_map.vtk');
bm = permute(bm, [2 1 3]);
gt_segments = bwlabeln(uint8(bm == 0), 6);
il_gt_idxs = {6:500, 6:500, 1:115};
il_as_idxs = {1:495, 1:495, 2:116};

ms = [2 4 8 16 32 64 128];
nms = numel(ms);
reps = 5;

il_t = 3:3:255;
num_thresh_il = numel(il_t);
il_conf = zeros([reps, nms, num_thresh_il, 2, 2]);

for k=1:nms,
    m = ms(k);
    for j=1:reps,
%        h = waitbar(0, sprintf('running il evaluation for %i markers', m));
        for i=1:num_thresh_il,
            il_seg = read_segmentation_from_raw([datadir 'ilastik.ws'], [datadir sprintf('ilastik.amb_%03i.raw', il_t(i))]);
            [il_conf(j,k,i,:,:), ~] = body_rand(gt_segments(il_gt_idxs{:}), il_seg(il_as_idxs{:}), struct('perform_watershed_gt', 0, 'perform_watershed_as', 0, 'markers_per_body', m, 'min_body_size', 3000, 'submatrix_multiplier', min([8, m]), 'diff', 0, 'debug', 0, 'verbose', 0));
%            waitbar(i/num_thresh_il, h);
        end
    end
%    delete(h)
    fprintf('done with %i markers', m);
end

