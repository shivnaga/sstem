function make_mex_here()
  mex -output correspond_segments_from_boundary_correspondence ...
    -I../../lib/hash_functions ...
    CFLAGS="\$CFLAGS -O3" ...
    correspond_segments_from_boundary_correspondence.cpp
return
end
