#!/usr/bin/env python
#
# copy_xml_images -- Copy images referenced in TrakEM2 XML file
#                    and optionally modify XML file.
# Copyright 2010 HHMI.  All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#     * Neither the name of HHMI nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Author: katzw@janelia.hhmi.org (Bill Katz)
#  Written as part of the FlyEM Project at Janelia Farm Research Center.
"""
copy_xml_images.py

Created by Katz, William on 2010-09-28.
Copyright (c) 2010 HHMI. All rights reserved.

Copy images referenced in TrakEM2 XML file and optionally duplicate and
modify the XML file to reference a path that can be symbolically linked
to another location.
"""

from __future__ import with_statement

import sys
import os
import os.path
import shutil
import re
import errno
from optparse import OptionParser

HELP_USAGE = """
Copy images referenced in TrakEM2 XML file to a target directory.
Optionally create a copy of the source XML file with some root file
path modified to some path that will be symbolically linked to the
target directory.

    %prog [options] <source XML> <dest dir>
"""

def make_dir(dir_name):
    try:
        os.makedirs(dir_name)
    except OSError as exc:
        if exc.errno == errno.EEXIST:
            pass
        else:
            raise

def copy_xml_images(source_xml, dest_dir, options):
    if options.replacedir and options.newpath:
        print("Since replacement path and new path are specified, this script will")
        print("copy an adjusted XML file to %s" % dest_dir)
        src_xml_filename, extension = os.path.splitext(os.path.basename(source_xml))
        new_xml_filename = os.path.join(dest_dir, src_xml_filename + "_modified" + extension)
        new_file = open(new_xml_filename, 'w')
    else:
        new_file = False
            
    # Process each line of the XML looking for "file_path"
    print("Copying all images referenced by %s to %s ..." % (source_xml, dest_dir))
    regex = re.compile('file_path="(?P<filepath>[^"]+)"');
    with open(source_xml, 'r') as f:
        for line in f:
            replace_line = line
            match = regex.search(line)
            if match:
                src_file_path = match.group('filepath')
                if options.replacedir:
                    pattern = options.replacedir
                    if not pattern.endswith('/'):
                        pattern += '/'
                    pattern += '(?P<relfilepath>.+)'
                    regex2 = re.compile(pattern)
                    match2 = regex2.search(src_file_path)
                    if match2:
                        dest_file = os.path.join(dest_dir, match2.group('relfilepath'))
                        replace_line = '   file_path="%s"' % os.path.join(options.newpath, match2.group('relfilepath'))
                        replace_line += "\n"
                    else:
                        print("Could not match stem directory for %s -- just copying file to %s..." % \
                              (src_file_path, dest_dir))
                        dest_file = os.path.join(dest_dir, os.path.basename(src_file_path))
                else:
                    dest_file = os.path.join(dest_dir, os.path.basename(src_file_path))
                # Copy referenced image to dest_dir
                if options.dryrun:
                    print("Dryrun: %s -> %s" % (src_file_path, dest_file))
                elif os.path.exists(dest_file):
                    print("File %s already exists.  Skipping." % (dest_file))
                elif os.path.exists(src_file_path):
                    file_dir, file_name = os.path.split(dest_file)
                    make_dir(file_dir)
                    shutil.copy(src_file_path, dest_file)
                    print("Copied %s -> %s" % (src_file_path, dest_file))
                else:
                    print("Referenced image %s cannot be found.  Skipping." % src_file_path)
            sys.stdout.flush()
            if new_file:
                new_file.write(replace_line)
    new_file.close()

def main():
    parser = OptionParser(usage=HELP_USAGE)
    parser.add_option("--replacedir",
                      action="store", type="string", dest="replacedir", metavar="DIR",
                      help="Path to replace with --newpath value")
    parser.add_option("--newpath",
                      action="store", type="string", dest="newpath", metavar="DIR_OR_SYMLINK",
                      help="Directory or symlink to use in XML instead of old path")
    parser.add_option("--dryrun",
                      action="store_true", dest="dryrun", metavar="TRUE|FALSE",
                      help="Just show files that will be copied without copying")

    (options, args) = parser.parse_args()

    if len(args) == 2:
        source_xml = args[0]
        dest_dir = args[1]
        
        if not os.path.exists(source_xml):
            print("Could not find source TrakEM XML: %s", source_xml)
            sys.exit(1)

        copy_xml_images(source_xml, dest_dir, options)
        print("Done.  Copied all images referenced by %s to %s" % (source_xml, dest_dir))
    else:
        parser.print_help()
        return -1


if __name__ == '__main__':
	main()

