function segmentation_stich_pipeline(config, main_module_id, sub_module_id) %#ok<INUSD>
if(config.is_verbose)
  fprintf('START: segmentation_stich_pipeline\n');
end
switch(main_module_id)
  case 650
    % If you have multiple overlapping tiles in one section and
    % you want to correspond segments in tile i with segments in
    % an overlapping region of tile j.
    replace_flag = 0;
    if(isfield(config, 'segmentation_2D'))
      segmentation_config = config.segmentation_2D;
      replace_flag = 1;
    end;
    config.segmentation_2D = config.superpixel_2_seg(end);
    align_segment_map_multi_tile(config);
    config = rmfield(config, 'segmentation_2D');
    if(replace_flag==1)
      config.segmentation_2D = segmentation_config;
    end;
  case 660
    error('This is deprecated you fool.  You should be running 670.');
    % generate identity mapping for locked labels.
    % useful when combining proofread volumes.
    % --- generate_identity_align_segment_mappings(config);
  case 670
    % Each segment is going to be mapped to a unique ID in the
    % joint index space for all tiles in a section.
    generate_nonoverlapping_align_segment_mappings(config);
end
if(config.is_verbose)
  fprintf('STOP: segmentation_stich_pipeline\n');
end
return
end
