function se = diamond_se(sz, d)
    sz = repmat(sz, [1 d]);
    se = zeros(sz);
    ctr = ceil(sz/2);
    for i=1:numel(se),
        if manhattan_distance(myind2sub(sz, i), ctr) <= floor(min(sz)/2),
            se(i) = 1;
        end
    end
end

function d = manhattan_distance(v1, v2)
    d = sum(abs(v1-v2));
end