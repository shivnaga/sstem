% This script compares two or more proofreader sessions and lists 
% significant body differences as computed by Dice's Coefficient.  See:
% Differences are output as a 3D tif with significant bodies highlighted
% in read, and a text file with the labels assigned by each proofreader
% to the variable regions.
%
% William T. Katz
% Janelia Farm Research Campus, HHMI
%
% v0   03242010   initial code

clear all
close all

highlight_dice_coeff = [20.0 80.0];
MAX_ARRAY_ELEMENTS = 250000000;  % Say we can handle 100M array element

grayscale_filename_format = '/groups/chklovskii/chklovskiilab/em_reconstruction/data_to_be_proofread/fly_larva.no_ua.5000xbinx2.0_17/analysis1/grayscale_maps/image.v0.%05d.png';
output_stack_filename = '~/compare_proofreading_lei_vic_20_80.tif';
output_text_filename = '~/compare_proofreading_lauchies_export84.8_20_80.txt';

% Proofreader sessions of interest go here.

session_dir.('launchies') = '/groups/chklovskii/chklovskiilab/em_reconstruction/proofread_data/comparison_of_proofreading/lauchies_81.0/';
body_ids.('launchies') = [1 3 5];
%session_dir.('export') = '/groups/chklovskii/chklovskiilab/em_reconstruction/proofread_data/medulla.HPF.Leginon.3500x.zhiyuan.fall2008/region.crop4_global_alignment_0161_1460.1281.1310.185929799407_40/ms3_1281.1310_4k.4k/export84.8%/';
session_dir.('mat') = '/groups/chklovskii/chklovskiilab/em_reconstruction/proofreading_sessions/saundersm/fly_larva.no_ua.5000xbinx2.0_17/analysis1/';
body_ids.('mat') = [2 6 8];
%session_dir.('victor') = '/groups/chklovskii/chklovskiilab/em_reconstruction/proofreading_sessions/shapirov/fly_larva.no_ua.5000xbinx2.0_17/analysis1/';

%% Choose which proofreaders to compare here
proofreaders = {'launchies', 'export'};
% proofreaders = fieldnames(session_dir);  % Compares all proofreaders
planes = 1281:1290;

%% Load the proofread stacks
num_proofreaders = length(proofreaders);
for reader = 1:num_proofreaders
    fprintf('Loading stack from %s ...\n', proofreaders{reader});
    BodyLabels{reader} = uint32(get_body_label_stack_from_raveler_proofread_data(...
      session_dir.(proofreaders{reader}), planes));
    fprintf('done.\n');
end

%% Preallocate structures that we can
stack_size = size(BodyLabels{1});
num_pixels = sub2ind(stack_size, stack_size(1), stack_size(2), stack_size(3));
LinearLabels(num_pixels, num_proofreaders) = 0;
num_bodies = zeros(num_proofreaders,1);
for reader = 1:num_proofreaders
    LinearLabels(:,reader) = BodyLabels{reader}(:) + 1;
    num_bodies(reader) = max(LinearLabels(:,reader));
end
max_num_bodies = max(num_bodies);

%% Calculate volume of intersections.

fprintf('Determining intersection volumes ...\n');
tic;
SortedLinearLabels = sortrows(LinearLabels);
[IntersectionLabels, first_indices, ~] = unique(SortedLinearLabels, 'rows', 'first');
num_overlap = size(IntersectionLabels,1);
[~, last_indices, ~] = unique(SortedLinearLabels, 'rows', 'last');
IntersectionVolumes = last_indices - first_indices + 1;
toc

%% Calculate volumes for each label

fprintf('Calculating volumes for each label ...\n');
tic;
LabelVolume{num_proofreaders} = 0;
for reader = 1:num_proofreaders
    SortedLinearLabels = sort(LinearLabels(:,reader));
    [UniqueLabels, first_indices, ~] = unique(SortedLinearLabels, 'first');
    [~, last_indices, ~] = unique(SortedLinearLabels, 'last');
    LabelVolume{reader} = zeros(num_bodies(reader),1);
    LabelVolume{reader}(UniqueLabels) = last_indices - first_indices + 1;
end
toc

%% Remove all lines where there's not one body ID of interest from a
%% proofreader.  -- TODO


%% Calculate average volumes for labels in an intersection

fprintf('Calculating average volumes for labels in an intersection ...\n');
tic;
AverageVolume = zeros(num_overlap,1);
for reader = 1:num_proofreaders
    AverageVolume = AverageVolume + ...
        LabelVolume{reader}(IntersectionLabels(:,reader));
end
AverageVolume = AverageVolume / num_proofreaders;
toc

%% Compute Dice's coefficient for each label intersection

fprintf('Calculating Dice''s coefficient for each label intersection ...\n');
tic;
DiceCoeff = (100 * IntersectionVolumes) ./ AverageVolume;
toc

%% Sort by Dice's coefficient for labels that overlap in some way.
%
% significant_labels = labels where each column is a proofreader

significant_indices = find(DiceCoeff >= highlight_dice_coeff(1) & ...
                        DiceCoeff <= highlight_dice_coeff(2));
% Remove the MATLAB index increment before output
SignificantLabels = IntersectionLabels(significant_indices, :) - 1;
SignificantScores = DiceCoeff(significant_indices);
LabelsAndVolumes(length(SignificantLabels), num_proofreaders*2) = 0;
for reader = 1:num_proofreaders
    LabelsAndVolumes(:,reader*2-1) = SignificantLabels(:,reader);
    LabelsAndVolumes(:,reader*2) = LabelVolume{reader}(SignificantLabels(:,reader)+1);
end

%% Output sorted Dice Coefficients and (body_id, volume) per reader.

[SortedScores, sort_index] = sort(SignificantScores);
header = 'Score\t';
dataformat = '%6.1f\t';
for reader = 1:num_proofreaders
    header = [header, proofreaders{reader}, '\t\t'];
    dataformat = [dataformat, '%i\t%i\t'];
end
header = [header, '\n'];
dataformat = [dataformat, '\n'];
fid = fopen(output_text_filename, 'w');
fprintf(fid, header);
fprintf(fid, dataformat, [SortedScores, LabelsAndVolumes(sort_index,:)]');
fclose(fid);

%% Output color images to show results

fprintf('Rendering difference stack for viewing ...\n');
if (exist(output_stack_filename, 'file')==2)
  delete(output_stack_filename);
end

for i = 1:length(planes)
    plane = planes(i);
    grayscale_image = uint8(imread(sprintf(grayscale_filename_format, plane)));
    [height, width] = size(grayscale_image);
    diff_image(height, width, 3) = uint8(0);  % Preallocate
    diff_image(:,:,2) = grayscale_image;
    diff_image(:,:,3) = grayscale_image;
    highlight_image = ones(height, width);
    for reader = 1:num_proofreaders
        highlight_image = highlight_image & ...
            ismember(BodyLabels{reader}(:,:,i), SignificantLabels(:,reader));
    end
    diff_image(:,:,1) = max(grayscale_image, uint8(255 * highlight_image));
    
    imwrite(diff_image, output_stack_filename, 'Compression', 'none',...
        'WriteMode', 'append');
end

fprintf('Finished writing difference images to %s.\n', output_stack_filename);
