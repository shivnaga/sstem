function linkage_3D_train_symm_diff_mean_boundary_lp_p3(config) %#ok<INUSD>
% linkage_3D_train_symm_diff_mean_boundary_lp_p3(config)
% Trains to perform 3D linkage.
% Method: minimize pixelwise symmetric difference between linked segments.
% Optimization performed using linear programming formulation. See
% "Clustering with qualitative constraints", Charikar et al.
%
% Shiv N. Vitaladevuni
% Janelia Farm Research Campus, HHMI
%
% v0  08012009  init code
%

fprintf('START: linkage_3D_train_symm_diff_mean_boundary_lp_p3\n');

fprintf('This method does not have training yet.\n');

fprintf('STOP: linkage_3D_train_symm_diff_mean_boundary_lp_p3\n');
return;
end
