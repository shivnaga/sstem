function compare_proofread_stacks_from_raveler(...
  section_image_name_format, planes, proofread_dir_a, proofread_dir_b, ...
  diff_stack_file_name, rendering_scale, render_original_image, ...
  varargin_1, varargin_2, only_corrected)
% compare_proofread_stacks_from_raveler(...
%   section_image_name_format, planes, proofread_dir_a, proofread_dir_b, ...
%   diff_stack_file_name, rendering_scale, render_original_image)
% Compare two proofreadings of a stack obtained from Raveler.
% The two proofreading sessions must first be exported from Raveler.
%
% Input:
%   section_image_name_format
%     specifies file name format with full path in printf format. E.g., if
%     the images were stored as /tmp/a.01.tif, /tmp/a.02.tif, ...
%     /tmp/a.42.tif, then the section_image_name_format should be
%     /tmp/a.%02d.tif and planes should be 1:42.
%   planes
%     list of the section ids.
%   proofread_dir_a/b
%     full path to directories having the exported data. The program
%     expects the superpixel maps to be stored in
%     proofread_dir_a/sp_maps/, and files superpixel-to-segment.txt
%   diff_stack_file_name
%     rendered diff-stack save file name.
%   rendering_scale
%     the amount by which to scale down the sections for rendering. Can be
%     useful for reducing storage space. Default 1.
%   render_original_image
%     whether to render the original image alongside the diff-stack for
%     easy viewing. Default false.
%
% Shiv N. Vitaladevuni
% Janelia Farm Research Campus, HHMI.
%
% v0  06192009  init. code
%

if(nargin<=5)
  rendering_scale = 1;
end
if(nargin<=6)
  render_original_image = false;
end

fprintf('Loading proofread stack a ...\n');
if(nargin<=7)
  seg_0 = uint32(get_body_label_stack_from_raveler_proofread_data(...
    proofread_dir_a, planes));
  fprintf('done.\n');
  fprintf('Loading proofread stack b ...\n');
  seg_1 = uint32(get_body_label_stack_from_raveler_proofread_data(...
    proofread_dir_b, planes));
  fprintf('done.\n');
else
  seg_0 = uint32(get_body_label_stack_from_raveler_proofread_data(...
    proofread_dir_a, planes, varargin_1));
  fprintf('done.\n');
  fprintf('Loading proofread stack b ...\n');
  seg_1 = uint32(get_body_label_stack_from_raveler_proofread_data(...
    proofread_dir_b, planes, varargin_2));
  fprintf('done.\n');
end

if(nargin < 10)
  only_corrected = true;
end

% save('~/temp/debug.mat', 'seg_0', 'seg_1');

if (only_corrected)
    corrected_bodies_fname = 'correctedbodies.txt';
    fprintf('Reading %s\n', [proofread_dir_a, corrected_bodies_fname]);
    fin_corrected_A = fopen([proofread_dir_a, corrected_bodies_fname], 'rt');
    corrected_A = fscanf(fin_corrected_A, '%d')';
    fclose(fin_corrected_A);
    fprintf('Reading %s\n', [proofread_dir_b, corrected_bodies_fname]);
    fin_corrected_B = fopen([proofread_dir_b, corrected_bodies_fname], 'rt');
    corrected_B = fscanf(fin_corrected_B, '%d')';
    fclose(fin_corrected_B);

    fprintf('Finding uncorrected bodies in slices ...\n');
    seg_0_uncorrected = ~ismember(seg_0, corrected_A);
    seg_1_uncorrected = ~ismember(seg_1, corrected_B);
    uncorrected_pixels = seg_0_uncorrected & seg_1_uncorrected;
    fprintf('Removing pixels not corrected by either proofreader ...\n');
    seg_0(uncorrected_pixels) = 0;
    seg_1(uncorrected_pixels) = 0;
end

fprintf('Computing difference between proofread stacks ...\n');
min_overlap_norm_threshold = 0.001;
[matched_pairs, diff_stack]  = ...
  compare_label_stacks_bp2(seg_0, seg_1, min_overlap_norm_threshold);
fprintf('done.\n');

fprintf('Rendering difference stack for viewing ...\n');
if(exist(diff_stack_file_name, 'file')==2)
  delete(diff_stack_file_name);
end
if(render_original_image)
  x_disp = 50;
else
  x_disp = 50;
end

for i = 1:length(planes)
  plane = planes(i);
  fprintf('plane: %d\n', plane);
  a = imread(sprintf(section_image_name_format, plane));
  
  [height, width] = size(a);
  height_2 = round(height/rendering_scale);
  width_2 = round(width/rendering_scale);
  a = imresize(a, [height_2, width_2]);

  d = imresize(diff_stack(:,:,i), [height_2, width_2], 'nearest');
  fprintf('num pink pixels: %d\n', nnz(bitand(d,uint8(1))>0 & bitand(d,uint8(4))>0));
  
  if(render_original_image)
    image = uint8(zeros([height_2, 2*width_2+x_disp, 3]));
    image(:,1:width_2,:) = repmat(a, [1 1 3]);
    image(:,width_2+x_disp+1:end, :) = repmat(a, [1 1 3]);
    
    image(:,width_2+x_disp+1:end, 1) = ...
      max(image(:,width_2+x_disp+1:end, 1), uint8(255*(bitand(d,uint8(1))>0)));
    image(:,width_2+x_disp+1:end, 2) = ...
      max(image(:,width_2+x_disp+1:end, 2), uint8(255*(bitand(d,uint8(2))>0)));
    image(:,width_2+x_disp+1:end, 3) = ...
      max(image(:,width_2+x_disp+1:end, 3), uint8(255*(bitand(d,uint8(4))>0)));
  else
    image = repmat(a, [1 1 3]);
    
    image(:,:, 1) = ...
      max(image(:,:, 1), uint8(255*(bitand(d,uint8(1))>0)));
    image(:,:, 2) = ...
      max(image(:,:, 2), uint8(255*(bitand(d,uint8(2))>0)));
    image(:,:, 3) = ...
      max(image(:,:, 3), uint8(255*(bitand(d,uint8(4))>0)));
  end
  
  imwrite(image, diff_stack_file_name, 'Compression', 'none', 'WriteMode', 'append');
end
fprintf('done.\n');

return
end
