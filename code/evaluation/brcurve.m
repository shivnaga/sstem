function [npv, spec] = brcurve(gt, ts, as_prefix, as_agglo_prefix, as_suffix, params, cv)

    if nargin < 7,
        cv = {1:size(gt, 1), 1:size(gt, 2), 1:size(gt, 3)};
    end    
    if nargin < 6,
        params = struct();
    end
    if nargin < 5,
        as_suffix = '';
    end
    if nargin < 4,
        as_agglo_prefix = '.amb_';
    end
    
    if ~isfield(params, 'dir'),
        dd = '/opt/local/br/';
    else
        dd = params.dir;
    end
    
    if ~isfield(params, 'padding'),
        format_str = '%i';
    else
        format_str = ['%0' sprintf('%i', params.padding) 'i'];
    end
    
    if ~isfield(params, 'reps'),
        reps = 1;
    else
        reps = params.reps;
    end
    
    if ~isfield(params, 'br_params'),
        br_params = struct('markers_per_body', 20, 'min_body_size', 1000, 'diff', 0, 'debug', 0, 'verbose', 0);
    else
        br_params = params.br_params;
    end
    
    if ~isfield(params, 'debug'),
        params.debug = false;
    end
    if params.debug,
        br_params.debug = true;
    end
    
    if ~isfield(params, 'verbose'),
        params.verbose = false;
    end
    if params.verbose,
        br_params.verbose = true;
    end
    
    if isfield(params, 'save_markers') && params.save_markers,
        br_params.save_markers = true;
    else
        br_params.save_markers = false;
    end
    
    gt = gt(cv{:});
    gtcc = bwconncomp(gt, 6);
    gtcc.ObjectSizes = connected_components_size_distribution(gtcc);
    gt = labelmatrix(gtcc);
    gtcc_no_erosion = gtcc;
    if isfield(params, 'erode_gt') && numel(params.erode_gt) > 1,
        gte = imerode(gt, params.erode_gt);
        for i=1:gtcc.NumObjects,
            gtcc.PixelIdxList{i} = find(gte == i);
        end
    end
    
    if isfield(params, 'diff_directory'),
        br_params.diff = true;
        br_params.diff_markers = false;
        br_params.save_markers = true;
        outdir = params.diff_directory;
        write_params = struct('single_file', false, 'type', 'png', 'bitdepth', 16);
        correct_boundaries = find_all_boundaries(gtcc_no_erosion, 10);
        correct_vol = find_separating_boundaries(gt, correct_boundaries, 'decreasing');
    else
        outdir = '';
    end
    
    conf = zeros([reps numel(ts) 2 2]);
    for k=1:reps,
        h = waitbar(0, sprintf(['running ' as_prefix ' evaluation number %i'], k));
        if br_params.save_markers,
            % note: labelmatrix(bwconncomp()) is necessary to make later
            % operations invariant in the label identities.
            seg = labelmatrix(bwconncomp(read_segmentation_from_raw([dd as_prefix '.ws'], ...
                [dd sprintf([as_prefix as_agglo_prefix format_str as_suffix], ...
                ts(1))]), 6));
            [~, ~, br_params.marker_locations] = body_rand(gtcc, seg(cv{:}), br_params);
        end
        for i=1:numel(ts),
            seg = labelmatrix(bwconncomp(read_segmentation_from_raw([dd as_prefix '.ws'], ...
                [dd sprintf([as_prefix as_agglo_prefix format_str as_suffix], ...
                ts(i))]), 6));
            [conf(k,i,:,:), diff] = body_rand(gtcc, seg(cv{:}), br_params);
            if ~isempty(outdir),
                if params.debug,
                    t1 = tic;
                end
                splits_vol = find_separating_boundaries(seg(cv{:}), diff.false_splits, 'decreasing', gtcc_no_erosion);
                merges_vol = find_separating_boundaries(gt, diff.false_merges, 'decreasing', bwconncomp(seg, 6));
                correct_vol(logical(merges_vol)) = 0;
                if params.debug,
                    fprintf('time spent finding boundaries for single threshold: %ds\n', toc(t1));
                end
                splits_channel = 2; merges_channel = 1; correct_channel = 3;
                fnsplits = sprintf('eval_t%03i_z%%03i_c%03i.png', i, splits_channel);
                fnmerges = sprintf('eval_t%03i_z%%03i_c%03i.png', i, merges_channel);
                fncorrect = sprintf('eval_t%03i_z%%03i_c%03i.png', i, correct_channel);
                write_image_stack(splits_vol, [outdir '/' fnsplits], write_params);
                write_image_stack(merges_vol, [outdir '/' fnmerges], write_params);
                write_image_stack(correct_vol, [outdir '/' fncorrect], write_params);
            end
            waitbar(i/numel(ts), h);
        end
        delete(h)
    end
    
    mean_conf = squeeze(mean(conf, 1));
    
    npv = mean_conf(:,2,2) ./ (mean_conf(:,2,2) + mean_conf(:,2,1));
    spec = mean_conf(:,2,2) ./ (mean_conf(:,2,2) + mean_conf(:,1,2));
end


function p = find_all_boundaries(cc, min_size)
    bodies = find(cc.ObjectSizes > min_size);
    discard = setdiff(1:cc.NumObjects, bodies);
    for i=discard,
        cc.PixelIdxList{i} = [];
    end
    v = double(labelmatrix(cc));
    vd = imdilate(v, diamond_se(3,3)) .* (v==0);
    v(v==0) = realmax;
    ve = imerode(v, diamond_se(3,3)) .* (v==realmax);
    isect = find(logical(vd) .* logical(ve));
    p = unique([ve(isect), vd(isect)], 'rows');
    p = p(p(:,1) ~= p(:,2), :);
end