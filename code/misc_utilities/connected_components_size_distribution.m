function sd = connected_components_size_distribution(cc)
    sd = zeros([cc.NumObjects 1]);
    for i=1:cc.NumObjects,
        sd(i) = numel(cc.PixelIdxList{i});
    end
end