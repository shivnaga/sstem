function idxs = get_neighbouring_indices(idx, mat_size)
    idxs = zeros([6 1]);
    c = 1;
    for i=1:3,
        for j=[-1,1],
            idx(i) = idx(i) + j;
            if all(idx ~= 0) && all(idx <= mat_size),
                idxs(c) = sub2ind(mat_size, idx(1), idx(2), idx(3));
            end
            c = c+1;
            idx(i) = idx(i) - j;
        end
    end
    idxs = idxs(idxs ~= 0);
end