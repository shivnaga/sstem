function v = sample_labels(label_vol, label_proportions, idxs)
    
    if nargin < 3,
        valid_idxs = 1:numel(label_vol); % we can sample from the full volume
    else
        valid_vol = zeros(size(label_vol));
        valid_vol(idxs{:}) = 1;
        valid_idxs = find(valid_vol);
    end
    if ~any(label_proportions == 1),
        label_proportions = label_proportions / max(label_proportions);
    end
    
    label_locations = cell(1,numel(label_proportions));
    label_sizes = zeros(size(label_proportions));
    for i=1:numel(label_proportions),
        if label_proportions(i) > 0,
            label_locations{i} = intersect(find(label_vol==i), valid_idxs);
            label_sizes(i) = numel(label_locations{i});
        end
    end
    label_relative_sizes = label_sizes / sum(label_sizes);
    sample_ratio = label_proportions ./ label_relative_sizes;
    sample_ratio = sample_ratio / max(sample_ratio);
    sample_ratio(isinf(sample_ratio)) = 0;
    num_samples = floor(sample_ratio .* label_sizes);
    v = zeros(size(label_vol), 'uint8');
    for i=1:numel(num_samples),
        v(randsample(label_locations{i}, num_samples(i))) = i;
    end
end