function write_ilastik_label_stack(labels, fn, group)
% write_ilastik_label_stack(labels, fn [, group])
% writes an HDF5 stack in Ilastik format. The default group is for the labels,
% 'DataSets/dataItem00/labels/data'. Provide a volume, filename, and optional
% group name.
% Known limitation: Matlab will automatically squeeze trailing singleton
% dimensions, so no effort is made to use Ilastik compatible shape (e.g.
% [1 500 400 300 1]). You should reshape using Python and h5py
    if nargin < 3,
        group = 'DataSets/dataItem00/labels/data';
    end
    hdf5write(fn, group, labels);
end