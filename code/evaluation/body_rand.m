function [conf_matrix, varargout] = body_rand(gt, as, params)
% [CONF_MATRIX, [DIFF, MARKERS]] = BODY_RAND(GT, AS [, PARAMS])
% Compute the Rand confusion matrix using a constant number of points
% (markers) per segmentation body.
% INPUT:
%   - GT: the ground truth segmentation
%   - AS: the automatic segmentation
%   - PARAMS: struct of options. Fields:
%      - gt_is_boundary_map: the ground truth is a boundary map
%      instead of a segmentation. (default: false)
%      - as_is_boundary_map: if the automatic segmentation is a boundary
%      map instead of a segmentation. (default: false)
%      - as_boundary_map_threshold: if the automatic segmentation boundary map has
%      continuous boundary values, threshold them by this threshold first.
%      (default: 1)
%      - is2d: perform a 2d analysis, do not assume nD connectivity.
%      - markers_per_body: how many points to select in each body.
%      (default: 10)
%      - min_body_size: disregard bodies smaller than this size. (default: 0)
%      - marker_locations: use the given marker locations instead of randomly
%      selecting new ones.
%      - save_markers: return the marker id locations for future use
%      - save_bodies: save the bodies that meet the minimum size requirement
%      - submatrix_multiplier: the number by which to multiply n (the number of
%      bodies) to get the submatrix size to store in memory.
%      - submatrix_size: how big of a matrix to store in memory for matrix
%      subtraction. Supersedes submatrix_multiplier.
%      - diff: whether or not to store the body_id pairs that differ between
%      ground truth and automatic segmentation
%      - diff_markers: output marker location pairs instead of body_id pairs
%      - verbose: print runtime information
%      - debug: print debugging information
% OUTPUT:
%   - CONF_MATRIX: Confusion matrix containing true positives, false positives,
%   false negatives, and true negatives.
%   - DIFF: (optional) the body ids or marker locations that are different
%   between ground truth and automatic segmentation. diff.false_merges are
%   together in as but not gt, while diff.false_splits are together in gt but
%   not as.
%   - MARKERS: (optional) the ids of markers used for body_rand evaluation.
%   Useful to repeat body_rand at different segmentation thresholds while 
%   maintaining the monotonicity of recall values.

    if ~exist('params', 'var'),
        params = struct();
    end
    if ~isfield(params, 'is2d'),
        params.is2d = false;
    end
    if ~isfield(params, 'gt_is_boundary_map'),
        params.gt_is_boundary_map = false;
    end
    if ~isfield(params, 'as_is_boundary_map'),
        params.as_is_boundary_map = false;
    end
    if ~isfield(params, 'as_boundary_map_threshold'),
        params.as_boundary_map_threshold = 1;
    end
    if ~isfield(params, 'markers_per_body'),
        params.markers_per_body = 10;
    end
    if ~isfield(params, 'min_body_size'),
        % you need at least the number of markers, plus some margin in case some
        % of those are 0-pixels in the AS.
        params.min_body_size = params.markers_per_body*2;
    end
    if ~isfield(params, 'save_markers'),
        params.save_markers = false;
    end
    if ~isfield(params, 'diff'),
        params.diff = false;
    end
    if ~isfield(params, 'diff_markers'),
        params.diff_markers = false;
    end
    if ~isfield(params, 'debug'),
        params.debug = false;
    end
    
    if params.gt_is_boundary_map,
        if mean(gt(:)) < 0.5, % attempts to determine inverse contrast
            gt = 1-gt;
        end
    end
    if params.as_is_boundary_map,
        as = uint8(as < params.as_boundary_map_threshold);
        if mean(as(:)) < 0.5,
            as = 1-as;
        end
        as = bwlabeln(as, conndef(ndims(as), 'minimal'));
    end
    
    if params.debug,
        t1 = tic;
    end
    
    if params.is2d,
        body_labels = unique(gt);
        body_label_counts = zeros(size(body_labels));
        for i=2:numel(body_labels),
            body_label_counts(i) = nnz(gt == body_labels(i));
        end
        body_labels = body_labels(body_label_counts >= params.min_body_size);
    else
        if ~isstruct(gt),
            gt = bwconncomp(gt, conndef(ndims(gt), 'minimal'));
        end
        if ~isfield(gt, 'ObjectSizes'),
            gt.ObjectSizes = connected_components_size_distribution(gt);
        end
        body_labels = find(gt.ObjectSizes > params.min_body_size);
    end
    as = labelmatrix(bwconncomp(as, 6));
    
    if params.debug,
        t2 = tic;
        fprintf('time elapsed for eliminating small bodies: %ds\n', toc(t1));
    end
    
    n = numel(body_labels);
    m = params.markers_per_body;
    pos = nchoosek(n, 2) * m^2;
    neg = nchoosek(m, 2) * n;
    
    if ~isfield(params, 'submatrix_multiplier'),
        params.submatrix_multiplier = 2;
    end
    if ~isfield(params, 'submatrix_size'),
        params.submatrix_size = params.submatrix_multiplier*n;
    end
    
    gtmarkers = zeros([n*m 1], 'double');
    asmarkers = zeros([n*m 1], 'double');
    if isfield(params, 'marker_locations'),
        markerids = params.marker_locations;
    else
        markerids = zeros([n*m 1]);
    end
    zero_as = find(as == 0);
    
    for i=1:n,
        if ~isfield(params, 'marker_locations'),
            if params.is2d,
                ids = randsample(setdiff(find(gt == body_labels(i)), zero_as), m); 
            else
                ids = randsample(setdiff(gt.PixelIdxList{body_labels(i)}, zero_as), m);
            end
            markerids(((i-1)*m+1):(i*m)) = ids;
        end
        gtmarkers(((i-1)*m+1):(i*m)) = body_labels(i);
        asmarkers(((i-1)*m+1):(i*m)) = as(markerids(((i-1)*m+1):(i*m)));
    end
    
    if params.debug,
        t3 = tic;
        fprintf('time elapsed for creating marker vectors: %ds\n', toc(t2));
        fprintf(['number of labels: %i,\n' ...
                'number of markers per label: %i,\n' ...
                'matrix size: %i,\n' ...
                'number of positive pairs: %i,\n' ...
                'number of negative pairs: %i,\n'], ...
                n, m, n*m, pos, neg);
    end
    
    s = params.submatrix_size;
    tp = 0;
    tn = 0;
    if params.diff,
        if params.diff_markers,
            diff = struct('false_splits', zeros([s^2 2*ndims(as)]), ...
                          'false_merges', zeros([s^2 2*ndims(as)]));
        else
            diff = struct('false_splits', zeros([s^2 3]), ...
                          'false_merges', zeros([s^2 3]));
        end
    else
        diff = 0;
    end
    fs_idx = 1;
    fm_idx = 1;
    if params.debug,
        t5 = 0;
    end
    for i=1:(n*m/s),
        for j=i:(n*m/s),
            gtij = repmat(gtmarkers(((i-1)*s+1):(i*s)), [1 s]);
            gttij = repmat(gtmarkers(((j-1)*s+1):(j*s)), [1 s])';
            dgt = uint8(logical(gtij - gttij));
            asij = repmat(asmarkers(((i-1)*s+1):(i*s)), [1 s]);
            astij = repmat(asmarkers(((j-1)*s+1):(j*s)), [1 s])';
            das = uint8(logical(asij - astij));
            if params.diff,
                [rx, ry] = find((1-das).*dgt);
                [px, py] = find(das.*(1-dgt));
            end
            if i == j,
                tp = tp + nnz(das.*dgt)/2;
                tn = tn + (nnz((1-das).*(1-dgt)) - s)/2;
                if params.diff,
                    validp = px < py;
                    px = px(validp);
                    py = py(validp);
                    validr = rx < ry;
                    rx = rx(validr);
                    ry = ry(validr);
                end
            else
                tp = tp + nnz(das.*dgt);
                tn = tn + nnz((1-das).*(1-dgt));
            end
            if params.diff,
                if params.debug,
                    t4 = tic;
                end
                nsplits = numel(px);
                nmerges = numel(rx);
                px = px + (i-1)*s;
                py = py + (j-1)*s;
                rx = rx + (i-1)*s;
                ry = ry + (j-1)*s;
                if fs_idx+nsplits-1 > size(diff.false_splits, 1),
                    % if we need more room, double the size of the array
                    diff.false_splits = vertcat(diff.false_splits, ...
                                        zeros(size(diff.false_splits)));
                end
                if params.diff_markers,
                    diff.false_splits(fs_idx:(fs_idx+nsplits-1),:) = ...
                        [myind2sub(size(as), markerids(px)) ...
                         myind2sub(size(as), markerids(py))];
                else
                    diff.false_splits(fs_idx:(fs_idx+nsplits-1),:) = ...
                        [asmarkers(px) asmarkers(py) gtmarkers(px)];
                end
                fs_idx = fs_idx + nsplits;
                if fm_idx+nmerges-1 > size(diff.false_merges, 1),
                    % if we need more room, double the size of the array
                    diff.false_merges = vertcat(diff.false_merges, ...
                                        zeros(size(diff.false_merges)));
                end
                if params.diff_markers,
                    diff.false_merges(fm_idx:(fm_idx+nmerges-1),:) = ...
                        [myind2sub(size(as), markerids(rx)) ...
                         myind2sub(size(as), markerids(ry))];
                else
                    diff.false_merges(fm_idx:(fm_idx+nmerges-1),:) = ...
                        [gtmarkers(rx) gtmarkers(ry) asmarkers(rx)];
                end
                fm_idx = fm_idx + nmerges;
                if params.debug,
                    t5 = t5 + toc(t4);
                end
            end
        end
    end
    
    if params.debug,
        fprintf('time elapsed computing statistics: %ds\n   of which in computing diff: %ds\n', toc(t3), t5);
    end
    
    if params.diff && ~params.diff_markers,
        if params.debug,
            t = tic;
        end
        diff.false_splits = unique(diff.false_splits(1:(fs_idx-1),:), 'rows');
        diff.false_merges = unique(diff.false_merges(1:(fm_idx-1),:), 'rows');
        if params.debug,
            fprintf('time computing uniquify: %ds\n', toc(t));
        end
    elseif params.diff && params.diff_markers,
        nd = ndims(as);
        diff.false_merges = diff.false_merges(diff.false_merges ~= 0);
        diff.false_merges = reshape(diff.false_merges, ...
                                [numel(diff.false_merges)/(2*nd) (2*nd)]);
        diff.false_splits = diff.false_splits(diff.false_splits ~= 0);
        diff.false_splits = reshape(diff.false_splits, ...
                                [numel(diff.false_splits)/(2*nd) (2*nd)]);
    end
    fp = neg - tn;
    fn = pos - tp;
    conf_matrix = [[tp fp]; [fn tn]];
    varargout = cell(2,1);
    if params.diff,
        varargout{1} = diff;
    end
    if params.save_markers,
        varargout{2} = markerids;
    end
end