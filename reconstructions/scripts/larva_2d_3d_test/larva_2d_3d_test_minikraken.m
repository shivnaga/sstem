
clear all

calculate_distance_transform = false;
aspect_ratio = [4 4 50];
edge_threshold = 50;  % Used for generating distance transform
min_edge_size = 10;   % Minimum size of edge connected component

overlap_linkage = true;
distance_linkage = false;

show_figures = false;
save_to_disk = true; %true;

stack_dir = '/groups/flyem/proj/data/tests/larva_2d_vs_3d/';
grayscale_template = 'grayscale/larva.%02d.png';
bm_template = 'boundary_maps4/larva.%02d.png_processed_bm.png';
mito_template = 'boundary_maps4/larva.%02d.png_processed_mito.png';
results_dir = '/groups/flyem/proj/data/tests/larva_2d_vs_3d/overlap_areas_edge4/';

check_for_dir(results_dir);

section_ids = 1:16;

disk3_se = strel('disk', 3);
disk4_se = strel('disk', 4);
disk5_se = strel('disk', 5);
disk8_se = strel('disk', 8);

superpixel_maps = {};
superpixel_to_segment_maps = {};
segment_offset = 0;
figure_num = 0;
for z = section_ids
  grayscale_filename = sprintf(grayscale_template, z);
  fprintf('Loading grayscale %d: %s\n', z, grayscale_filename);
  
  grayscale = imread([stack_dir, grayscale_filename]);
  if show_figures
      figure_num = figure_num + 1;
      figure(figure_num);
      imshow(grayscale);
      title('Grayscale');
  end
 
  bm_filename = sprintf(bm_template, z);
  fprintf('Loading boundary map %d: %s\n', z, bm_filename);
  
  bmraw = imread([stack_dir, bm_filename]);
  bm = 1 - im2double(bmraw);
  bmraw = 255 - bmraw;
%   bm = medfilt2(bm, [5 5]);
  if save_to_disk
      imwrite(bm, [results_dir, sprintf('boundary_map.%02d.png', z)]);
  end
  if show_figures
      figure_num = figure_num + 1;
      figure(figure_num);
      imshow(bm);
      title('Boundary Map');
  end
  
  mito_filename = sprintf(mito_template, z);
  fprintf('Loading mito map %d: %s\n', z, mito_filename);
  mito = 255 - imread([stack_dir, mito_filename]);
  if save_to_disk
      imwrite(mito, [results_dir, sprintf('mito_map.%02d.png', z)]);
  end
  if show_figures
      figure_num = figure_num + 1;
      figure(figure_num);
      imshow(mito);
      title('Mitochondria Map');
  end
  
  % Close the boundary map for watershed calc.
  bm = imclose(bm, disk5_se);
  if save_to_disk
      imwrite(bm, [results_dir, sprintf('boundary_map_closed.%02d.png', z)]);
  end
  if show_figures
      figure_num = figure_num + 1;
      figure(figure_num);
      imshow(bm);
      title('Boundary Map after closing with disk 5');
  end
  
  fprintf('Computing superpixel map ...\n');
  seeds = (bm < 0.2);
  seeds = imerode(seeds, disk4_se);
  seeds = imopen(seeds, disk3_se);
  seeds = bwareaopen(seeds, 25);
  if save_to_disk
      imwrite(seeds, [results_dir, sprintf('boundary_map_seeds.%02d.png', z)]);
  end
  if show_figures
      figure_num = figure_num + 1;
      figure(figure_num);
      imshow(seeds);
      title('Boundary Map Seeds');
  end
  
  % Add very likely mito areas to our seeds
  definite_mito = imerode(imclose((mito > 150), disk8_se), disk5_se);
  seeds = seeds | definite_mito;
  if save_to_disk
      imwrite(definite_mito, [results_dir, sprintf('mito_seeds.%02d.png', z)]);
  end
  if show_figures
      figure_num = figure_num + 1;
      figure(figure_num);
      imshow(definite_mito);
      title('Mitochondria Seeds');
  end
   
  bm_seeded = imimposemin(bm, seeds);
  if save_to_disk
      imwrite(bm_seeded, [results_dir, sprintf('bmap_imposemin.%02d.png', z)]);
  end
  if show_figures
      figure_num = figure_num + 1;
      figure(figure_num);
      imshow(bm_seeded);
      title('Boundary Map ImposeMin');
  end
  
  superpixel_map = watershed(bm_seeded);
  superpixel_maps{z} = superpixel_map; %#ok<SAGROW>
  figure_num = figure_num + 1;
  f = plot_segment_boundaries(grayscale, superpixel_map, 1, figure_num, show_figures);
  figure_title = sprintf('Superpixel map %d', z);
  title(figure_title);
  if save_to_disk
      print(f, '-dpng', [results_dir, sprintf('superpixel_map.%02d.png', z)]);
  end
  
  fprintf('Computing superpixel-to-segment map ...\n');
  minimum_boundary_threshold = 0.2;  % Reducing this increases oversegmentation
  minimum_area_threshold = 400;
  [label_map, sp_to_seg] = compute_segmentation_from_superpixels_with_min_area_c(...
    superpixel_map, bm_seeded, minimum_boundary_threshold, minimum_area_threshold);
  segment_map = double(remove_merged_boundaries_2D(uint32(label_map)));
  
  sp_to_seg(:,2) = sp_to_seg(:,2) + segment_offset;
  segment_offset = max(sp_to_seg(:,2));
  
  superpixel_to_segment_maps{z} = sp_to_seg; %#ok<SAGROW>
  figure_num = figure_num + 1;
  f = plot_segment_boundaries(grayscale, segment_map, 1, figure_num, show_figures);
  figure_title = sprintf('Segment map %d min area %d', z, minimum_area_threshold);
  title(figure_title);
  if save_to_disk
      print(f, '-dpng', [results_dir, sprintf('segment_map.%02d.png', z)]);
  end
end

fprintf('Writing grayscale images ...\n');
rav_gs_dir = [results_dir, 'grayscale_maps/'];
check_for_dir(rav_gs_dir);
for z = section_ids
  grayscale_filename = sprintf(grayscale_template, z);
  grayscale = imread([stack_dir, grayscale_filename]);
  imwrite(grayscale(:,:,1), [rav_gs_dir, 'image.', num2str(z, '%05d'), '.png']);
end
   
fprintf('Writing superpixel map ...\n');
rav_sp_dir = [results_dir, 'superpixel_maps/'];
check_for_dir(rav_sp_dir);
for z = section_ids
  imwrite(uint16(superpixel_maps{z}), [rav_sp_dir, 'sp_map.', num2str(z, '%05d'), '.png'], ...
    'BitDepth', 16);
end

fprintf('Writing superpixel to segment map ...\n');
fout = fopen([results_dir, 'superpixel_to_segment_map.txt'], 'wt');
for z = section_ids
  fprintf(fout, '%d\t%d\t%d\n', ...
    [repmat(z, [1 1+ size(superpixel_to_segment_maps{z}, 1)]); ...
    zeros(2, 1), superpixel_to_segment_maps{z}']);
end
fclose(fout);

% Calculate a 3D distance transform using anisotropic 3D data.
if calculate_distance_transform
    % Compute edge map used for distance transform.
    edge_map = max(bmraw, mito);
    edge_binary = (edge_map > edge_threshold);
    cc = bwconncomp(edge_binary);
    for i = 1:cc.NumObjects
        if size(cc.PixelIdxList{i}) < min_edge_size
            edge_binary(cc.PixelIdxList{i}) = false;
        end
    end
    if save_to_disk
        imwrite(edge_map, [results_dir, sprintf('edge_map.%02d.png', z)]);
        imwrite(edge_binary, [results_dir, sprintf('edge_binary.%02d.png', z)]);
    end
    if z == section_ids(1)  % Preallocate edge_3d_binary size.
        z_dim = size(section_ids, 2);
        edge_3d_binary = zeros(size(edge_map, 1), size(edge_map, 2), z_dim);
    end
    edge_3d_binary(:,:,z) = edge_binary;
      
    fprintf('Calculating 3D distance transform ...\n');
    dist = bwdistsc(edge_3d_binary, aspect_ratio);
    z_index = 1;
    for z = section_ids
        imwrite(uint8(2*dist(:,:,z_index)), [results_dir,...
            sprintf('edge_3d_binary.%02d.png', z)]);
        z_index = z_index + 1;
    end
end

% Linkage is based on connected components of distance transform
if distance_linkage && section_ids(1) ~= section_ids(end)
    fprintf('Performing segment and body mapping using distance transform ...\n');
    
    dist_binary = (dist > 50);
    cc = bwconncomp(dist_binary);
    for i = 1:cc.NumObjects
        
      cc = bwconncomp(edge_binary);
      for i = 1:cc.NumObjects
          if size(cc.PixelIdxList{i}) < min_edge_size
              edge_binary(cc.PixelIdxList{i}) = false;
          end
      end
    end
    links = [];
    z_1 = section_ids(1);
    segment_map_1 = apply_mapping(superpixel_maps{z_1}, superpixel_to_segment_maps{z_1});
    segment_map_1 = double(remove_merged_boundaries_2D(uint32(segment_map_1)));
    stats_1 = regionprops(segment_map_1);

    for z_1 = section_ids(1:end-1)
      z_2 = z_1+1;

      segment_map_2 = apply_mapping(superpixel_maps{z_2}, superpixel_to_segment_maps{z_2});
      segment_map_2 = double(remove_merged_boundaries_2D(uint32(segment_map_2)));
      stats_2 = regionprops(segment_map_2);

      label_pairs_pixels = [segment_map_1(:), segment_map_2(:)];
      [label_pairs, overlap_areas] = count_row_occurence(label_pairs_pixels);

      % Only use overlaps where neither segment is label 0
      all_overlaps = [label_pairs, overlap_areas];
      overlaps = all_overlaps(all_overlaps(:,1)>0 & all_overlaps(:,2)>0, :);
      num_non_edge_overlaps = size(overlaps, 1);
      
      % Get area for each segment involved in an overlap
      segment1_area = [stats_1(uint32(overlaps(:,1))).Area];
      segment2_area = [stats_2(uint32(overlaps(:,2))).Area];

      % Restrict overlaps to where overlap area > thresh * segment areas.
      overlaps = overlaps(overlaps(:,3) > thresh * segment1_area(:) & ...
          overlaps(:,3) > thresh * segment2_area(:), :);
      
      % Store all overlaps in one array.
      links = [links; overlaps]; %#ok<AGROW>
      
      fprintf('Computed %d of %d overlaps should be linked between slices %d and %d.\n',...
          size(overlaps,1), num_non_edge_overlaps, z_1, z_2);
      segment_map_1 = segment_map_2;
      stats_1 = stats_2;
    end

    max_segment_id = segment_offset;
    segment_to_body_map = [1:max_segment_id ; 1:max_segment_id];

    % Point all linked segments to the lower segment ID
    for row = size(links,1):-1:1
        segmentB = links(row, 2);
        segmentA = links(row, 1);
        segment_to_body_map(2, segmentB) = segmentA;
    end
    
    % For each entry, follow chain until we arrive at lowest number, which
    % we use.
    for segment = max_segment_id:-1:1
        s = segment;
        while (segment_to_body_map(2, s) ~= s)
            s = segment_to_body_map(2, s);
        end
        segment_to_body_map(2, segment) = s;
    end
        
    fprintf('Writing segment to body map ...\n');
    fout = fopen([results_dir, 'segment_to_body_map.txt'], 'wt');
    fprintf(fout, '%d\t%d\n', [zeros(2,1), segment_to_body_map]);
    fclose(fout);
end

% Linkage is based on overlap area relative to total segment area.
% If the overlap between two segments adjacent along z is large relative to
% the area of both segments, merge.  If not, just leave for now since it's
% better than overlinking.
if overlap_linkage && section_ids(1) ~= section_ids(end)
    for overlap_threshold = 0:.01:1
        fprintf('Performing linkage requiring %d%% overlap areas ...\n', ...
            round(overlap_threshold * 100));
        links = [];
        z_1 = section_ids(1);
        segment_map_1 = apply_mapping(superpixel_maps{z_1}, superpixel_to_segment_maps{z_1});
        segment_map_1 = double(remove_merged_boundaries_2D(uint32(segment_map_1)));
        stats_1 = regionprops(segment_map_1);

        for z_1 = section_ids(1:end-1)
          z_2 = z_1+1;

          segment_map_2 = apply_mapping(superpixel_maps{z_2}, superpixel_to_segment_maps{z_2});
          segment_map_2 = double(remove_merged_boundaries_2D(uint32(segment_map_2)));
          stats_2 = regionprops(segment_map_2);

          label_pairs_pixels = [segment_map_1(:), segment_map_2(:)];
          [label_pairs, overlap_areas] = count_row_occurence(label_pairs_pixels);

          % Only use overlaps where neither segment is label 0
          all_overlaps = [label_pairs, overlap_areas];
          overlaps = all_overlaps(all_overlaps(:,1)>0 & all_overlaps(:,2)>0, :);
          num_non_edge_overlaps = size(overlaps, 1);

          % Get area for each segment involved in an overlap
          segment1_area = [stats_1(uint32(overlaps(:,1))).Area];
          segment2_area = [stats_2(uint32(overlaps(:,2))).Area];

          % Restrict overlaps to where overlap area > thresh * segment areas.
          overlaps = overlaps(overlaps(:,3) > overlap_threshold * segment1_area(:) & ...
              overlaps(:,3) > overlap_threshold * segment2_area(:), :);

          % Store all overlaps in one array.
          links = [links; overlaps]; %#ok<AGROW>

          fprintf('Computed %d of %d overlaps should be linked between slices %d and %d.\n',...
              size(overlaps,1), num_non_edge_overlaps, z_1, z_2);
          segment_map_1 = segment_map_2;
          stats_1 = stats_2;
        end

        max_segment_id = segment_offset;
        segment_to_body_map = [1:max_segment_id ; 1:max_segment_id];

        % Point all linked segments to the lower segment ID
        for row = size(links,1):-1:1
            segmentB = links(row, 2);
            segmentA = links(row, 1);
            segment_to_body_map(2, segmentB) = segmentA;
        end

        % For each entry, follow chain until we arrive at lowest number, which
        % we use.
        for segment = max_segment_id:-1:1
            s = segment;
            while (segment_to_body_map(2, s) ~= s)
                s = segment_to_body_map(2, s);
            end
            segment_to_body_map(2, segment) = s;
        end

    %     % Relabel the mapping
    %     [seg_1, relabelling_1] = relabel_to_remove_nonexistent_labels(...
    %         segment_2D_label_map_2{tile_2}.label_map_t);
    %       relabelling_1(2:end) = relabelling_1(2:end) + max(relabelling_0);
    % 

        fprintf('Writing segment to body map ...\n');
        fout = fopen([results_dir, 'segment_to_body_map.',...
            sprintf('%03d', 100 - round(overlap_threshold*100)), '.txt'], 'wt');
        fprintf(fout, '%d\t%d\n', [zeros(2,1), segment_to_body_map]);
        fclose(fout);
    end
end