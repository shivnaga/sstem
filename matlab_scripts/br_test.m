
%% read in data and set up parameters

dd = '/opt/local/br/';

bm = readVTK([dd 'bm.vtk']);
bm = permute(bm, [2 1 3]);
bm = bm(:,:,1:115);
gt_segments = bwlabeln(uint8(bm == 0), 6);
gt_segments = imerode(gt_segments, diamond_se(5, 3));

cv = {11:490, 11:490, 11:105}; % crop the edges from the evaluation

reps = 5;

il_t = 3:3:252;
num_thresh_il = numel(il_t);
il_conf = zeros([reps, num_thresh_il, 2, 2]);

mf_t = 76:2:172;
num_thresh_mf = numel(mf_t);
mf_conf = zeros([reps, num_thresh_mf, 2, 2]);

lda_t = 85:2:159;
num_thresh_lda = numel(lda_t);
lda_conf = zeros([reps, num_thresh_lda, 2, 2]);

%% perform the analysis

for k=1:reps,
    h = waitbar(0, sprintf('running il evaluation number %i', k));
    for i=1:num_thresh_il,
        il_seg = read_segmentation_from_raw([dd 'il.ws'], [dd sprintf('il.amb_%03i', il_t(i))]);
        [il_conf(k,i,:,:), ~] = body_rand(gt_segments(cv{:}), il_seg(cv{:}), struct('as_is_boundary_map', 0, 'markers_per_body', 20, 'min_body_size', 3000, 'diff', 0, 'debug', 0, 'verbose', 0));
        waitbar(i/num_thresh_il, h);
    end
    delete(h)
end

for k=1:reps,
    h = waitbar(0, sprintf('running mf evaluation number %i', k));
    for i=1:num_thresh_mf,
        mf_seg = read_segmentation_from_raw([dd 'mf3.ws'], [dd sprintf('mf3.amb_%03i', mf_t(i))]);
        [mf_conf(k,i,:,:), ~] = body_rand(gt_segments(cv{:}), mf_seg(cv{:}), struct('as_is_boundary_map', 0, 'markers_per_body', 20, 'min_body_size', 3000, 'diff', 0, 'debug', 0, 'verbose', 0));
        waitbar(i/num_thresh_mf, h);
    end
    delete(h)
end

for k=1:reps,
    h = waitbar(0, sprintf('running lda evaluation number %i', k));
    for i=1:num_thresh_lda,
        seg = read_segmentation_from_raw([dd 'lda.ws'], [dd sprintf('lda.amb_%03i', lda_t(i))]);
        [lda_conf(k,i,:,:), ~] = body_rand(gt_segments(cv{:}), seg(cv{:}), struct('as_is_boundary_map', 0, 'markers_per_body', 20, 'min_body_size', 3000, 'diff', 0, 'debug', 0, 'verbose', 0));
        waitbar(i/num_thresh_il, h);
    end
    delete(h)
end

%% negative precision/recall computation (aka npv/spec)
% 
mean_il = squeeze(mean(il_conf, 1));
mean_mf = squeeze(mean(mf_conf, 1));
mean_lda = squeeze(mean(lda_conf, 1));

mf_npv = mean_mf(:,2,2) ./ (mean_mf(:,2,2) + mean_mf(:,2,1));
mf_spec = mean_mf(:,2,2) ./ (mean_mf(:,2,2) + mean_mf(:,1,2));
il_npv = mean_il(:,2,2) ./ (mean_il(:,2,2) + mean_il(:,2,1));
il_spec = mean_il(:,2,2) ./ (mean_il(:,2,2) + mean_il(:,1,2));
lda_npv = mean_lda(:,2,2) ./ (mean_lda(:,2,2) + mean_lda(:,2,1));
lda_spec = mean_lda(:,2,2) ./ (mean_lda(:,2,2) + mean_lda(:,1,2));

plot(mf_spec, mf_npv, '-k', lda_spec, lda_npv, '-g', il_spec, il_npv, '-r')

