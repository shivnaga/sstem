

d = '/groups/scheffer/home/schefferl/Pictures/temp/shinya161-1460/';

fout = fopen('~/temp/medulla_161_1460_12k_12k_tiles.txt', 'wt');
for i = 161:1460
    fprintf('i: %d\n', i);
    [s, r] = system(['cat ', d, 'tiles-', num2str(i), '.o* |   grep OK.*mrc.tif | awk ''{print ', ...
        ' substr($4, 72, length($4)-72);}''']);
    c = strsplit(r);
    for j = 2:length(c)
        dl = strfind(c{j}, '/');
        fprintf(fout, '%d %s %s\n', i, c{j}(1:dl(2)), c{j}(1:end-4));
    end
end
fclose(fout);
