#!/usr/bin/env python
"""Run watershed_3D_myers with negative images as default.

EM images typically show dark boundaries, but watershed and its derivatives 
require light boundaries. This wrapper allows one to directly input microscopy
images as default.

Author: Juan Nunez-Iglesias <nuneziglesiasj@janelia.hhmi.org>
"""

import os
import sys
from wrapper_utils.command_line_modifiers import add_negative_filter, \
                                                                    run_command

if __name__ == '__main__':
    rundir = os.path.dirname(os.path.realpath(sys.argv[0]))
    binary = rundir + '/watershed_3D_myers'
    run_command(binary, sys.argv, add_negative_filter)
