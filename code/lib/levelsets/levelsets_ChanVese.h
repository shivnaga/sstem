
#ifndef __LEVELSETS_CHANVESE__
#define __LEVELSETS_CHANVESE__

void ChanVese(double *phi, double *phiNew, double *Im, double epsilon, double nu, double delt, int r, int c, double c1, double c2, double lambda1, double lambda2, int h, int n_iteration);

#endif
